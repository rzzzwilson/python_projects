The Trac64 implementation here follows the description in the 1966  paper
reasonably closely, except where the python language made a different
implementation too attractive to ignore.

Active & Neutral Strings
------------------------

Instead of using a string (an array of characters) to store the active
and neutral strings, the code here uses a list of characters for both the
active and neutral strings.  This is the only sensible choice given that 
strings are immutable in python.

Argument Pointers
-----------------

The 1966 description uses a list of pointers into the neutral string to mark
the argument strings.  A separate stack-like data structure remembered the 
index into the neutral string of the funcion name string as well as the
function type.

This implementation combines both of those data structures into one: a list
that is used like a stack.  The beginning of a function (active or neutral)
pushes a "beginning" sublist to the stack.  The sublist contains the function
type and the index into the neutral string of the beginning of the function
name string.  Every additional argument beginning index is appended to the
sublist on the top of the stack.

When a function is executed the top sublist is broken up into the function
type, which is remembered, and a list of complete argument strings.  The
first element in the arguments list is the function name with the rest of the
list being the actual arguments, which is passed to the builtin code.

Form Storage
============

The form storage is a python dictionary, with the key being the form name
and the value is the "form" itself.

A form is a 2-part sequence.  The first element is the Form Pointer and the
second element is the actual Form.

You can see this structure of a form by looking in the *__env__.trac* file
or into any file created by *(sb,...)*.  The save fikes are JSON format.

Forms
-----

A form is a sequence of elements.  Segments in the form are strings and the
gaps are integers with the value used when they were created.  The first gap
created by the `#(ss,...)` function has the numeric value of 0, and so on.
As an example, if a form of name `test` originally contained the string 
`abcdefghijk` and was subsequently segmented by `#(ss,test,d,i)` then the 
form list would be::

    ["abc", 0, "efgh", 1, "jk"]

Form Pointer
------------

A form pointer associated with a form is a 2-part sequence with the first
element being the index of the segment containing the character pointed to,
and the second part is the index in the segment string of the character being
pointed to.  So after creating the segmented form above, the form pointer for
`test` would be `[0, 0]`::

    ["abc", 0, "efgh", 1, "jk"]
      ^
      |  the form pointer

After executing `#(cs,test)` to read one segment the form pointer would change
to `[2, 0]`, represented like this::

    ["abc", 0, "efgh", 1, "jk"]
                ^
                |  the form pointer


and the output from `#(pf)` would show::

    "test" | "abc{0}efgh{1}jk"
                    ^

Note that a form pointer should always point at a character in a text segment.
Builtin functions that create segments in a form must ensure that if the form
pointer was pointing at text that is now a gap after the operation, the form
pointer must be adjusted to be "sane".  Probably move forward to the next
text segment, or an EOF position if there is no text to the right.

Saved Environment
=================

An addition to the implementation is the automatic save/restore of the form
storage.  When Trac64 starts a file *__env__.trac* is read, if it exists, and
the form storage initialized with the data from the file.  Similarly, when
Trac64 closes the form storage is saved (as JSON) into *__env__.trac*.
