#!/usr/bin/env python

"""
An implementation of Trac64 as defined in the paper:

"TRAC, A Procedure-Describing Language for the Reactive Typewriter."
Calvin N. Mooers, Communications of the ACM, Volume 9, Number 3, March 1966.

Implementation follows the description in the paper, with the old-school
"goto stateX" replaced by a state-machine, ie, "virtual gotos"!

EXCEPT: Changed implementation to not use the "func ptr" and "arg ptr" lists
which follow the design in the paper.  Instead, keep one list "stack" that
contains sublists of arg indices including the 0th arg (the function name).
That simplifies handling of args/func indices, just use the list at the end
of "stack", append indices to it and pop it off after executing the function.
"""

import os
import sys
import json
import atexit
from itertools import zip_longest

HaveReadline = True
try:
    import readline
except (ImportError, AttributeError):
    # Not installed or running on Windows
    HaveReadline = False


TracVersion = "0.1"

IDLE_STRING = '#(ps,#(rs))'
ACTIVE = True
NEUTRAL = False

DEFAULT_SAVE = '__env__.trac'

# map a 3 digit boolean string to octal
bool2oct = {'000': '0', '001': '1', '010': '2', '011': '3',
            '100': '4', '101': '5', '110': '6', '111': '7'}

# map %3 remainder to list to extend octal to multiple of three groups
ljustoct = {0: [], 1: [False, False], 2: [False]}



class TracEngine:
    """A class for a Trac64 machine."""

    def __init__(self, commands=None, nosave=False, quiet=False, trace=False):
        """Initialize the engine."""

        self.commands = commands
        self.nosave = nosave
        self.quiet = quiet
        self.trace = trace

        self.meta = "'"                         # the initial "meta" character
        self.input_buff = ''                    # input not yet processed
        self.forms = {}

        # define all builtin functions
        self.builtins = {# vvvvv Trac builtins vvvvv
                         'rs': self.rs_builtin, 
                         'rc': self.rc_builtin,
                         'cm': self.cm_builtin, 
                         'ps': self.ps_builtin, 
                         'ds': self.ds_builtin, 
                         'ss': self.ss_builtin, 
                         'cl': self.cl_builtin, 
                         'cs': self.cs_builtin,
                         'cc': self.cc_builtin,
                         'cn': self.cn_builtin,
                         'in': self.in_builtin,
                         'cr': self.cr_builtin,
                         'dd': self.dd_builtin, 
                         'da': self.da_builtin,
                         'ad': self.ad_builtin, 
                         'su': self.su_builtin,
                         'ml': self.ml_builtin, 
                         'dv': self.dv_builtin,
                         'bu': self.bu_builtin, 
                         'bi': self.bi_builtin,
                         'bc': self.bc_builtin, 
                         'bs': self.bs_builtin,
                         'br': self.br_builtin, 
                         'eq': self.eq_builtin, 
                         'gr': self.gr_builtin, 
                         'sb': self.sb_builtin,
                         'fb': self.fb_builtin,
                         'eb': self.eb_builtin,
                         'ln': self.ln_builtin, 
                         'pf': self.pf_builtin, 
                         'tn': self.tn_builtin, 
                         'tf': self.tf_builtin,
                         # vvvvv extended builtins vvvvv
                         'wf': self.wf_builtin,
                         'rf': self.rf_builtin,
                         'exit': self.exit_builtin,
                        }

        # if there is a 'commands' file, prepare to read from it
        if self.commands:
            self.prev_trace = self.trace
            self.trace = False  # no trace while reading file

            try:
                sys.stdin = open(os.path.expanduser(self.commands))
                self.prev_quiet = self.quiet
                self.quiet = True
            except (FileNotFoundError, PermissionError, IsADirectoryError) as e:
                self.warn(f"Error opening commands file: {e}")
                sys.exit(2)

    ######
    # Utility methods
    ######

#    def debug(self, msg):
#        print(f"debug: {msg}")

    def warn(self, msg):
        """Print a warning message."""

        if not self.quiet:
            print(f"***** WARNING: {msg}")

    def split_prefix_num(self, s):
        """Strip prefix from numeric arg string.

        Returns a tuple (prefix, num).
        """

        num = 0
        prefix = s
        for (i, ch) in enumerate(s):
            if s[i:].isnumeric():
                num = int(s[i:])
                prefix = s[:i]
                if s[i-1:i] == '-':
                    num = -num
                    prefix = s[:i-1]
                break

        return (prefix, num)

    def split_fn_args(self, arg_ptrs, neutral):
        """Get the called function name + args from "arg_ptrs" and "neutral".

        arg_ptrs  indices into neutral of fname & arg string start
        neutral   the neutral string

        Returns (fname, args).
        """

        # create list of args from fb_index -> end
        args = []
        for (arg_index, next_arg) in zip_longest(arg_ptrs, arg_ptrs[1:],
                                                 fillvalue=len(neutral)):
            if arg_index != next_arg:
                args.append(''.join(neutral[arg_index:next_arg]))

        # get arg 0, the function name string
        fname = ''
        if args:
            fname = args.pop(0)

        return (fname, args)

    def linearize_form(self, form):
        """Return a single string ignoring segments in a form."""

        return ''.join(elt for elt in form if isinstance(elt, str))

    def move_fp(self, num, form):
        """Return a form pointer pointing at the "num"th char in a form.

        Returns a tuple (segnum, offset) which is a form pointer.
        Skips segments.  If "num" to large, return EOF FP.
        """

        # check each segment
        for (segnum, elt) in enumerate(form):
            if isinstance(elt, str):
                # does FP point into this segment?
                if num < len(elt):
                    # if so, return FP
                    return (segnum, num)

                # no, adjust length for next segment
                num -= len(elt)
            # string or numeric segment, try next

        # set FP to EOF for this form
        return (len(form), 0)

    def linearize_fp(self, fp, form):
        """Return a "linear" offset for an FP into a form.

        fp    the tuple (segnum, offset) of an FP
        form  the form list

        Returns the index into a linearized form that matches
        the actual FP position in the form.
        """

        (fp_seg, fp_off) = fp

        num = 0
        for (segnum, elt) in enumerate(form):
            if isinstance(elt, str):
                if segnum == fp_seg:
                    # FP is in this element
                    return num + fp_off
                num += len(elt)
        return num

    def normalize_fp(self, fp, form):
        """Return a "sane" form pointer given an FP into a form.

        fp    the existing form pointer
        form  the form

        Returns a {segnum, offset) form pointer into the form.
        """

        # get current form pointer segment and offset
        (segnum, offset) = fp
        if segnum >= len(form):
            # already at EOF, leave it
            return fp

        while segnum < len(form) and not isinstance(form[segnum], str):
            segnum += 1
            offset = 0

            if segnum >= len(form):
                break

        return (segnum, offset)
    
    def octal_to_bool(self, s):
        """Get a boolean list from an octal string.

        Returns a a list of True/False values.
        EG, 5 -> [True, False, True]
        """

        num = 0 
        for (i, ch) in enumerate(s):
            try:
                num = int(s[i:], base=8)
                if num >= 0:
                    break
            except ValueError:
                # not an octal string
                pass
    
        num = f"{num:b}"
        result = [True if digit == '1' else False for digit in num]

        return result

    def bool_to_octal(self, val):
        val = ljustoct[len(val) % 3] + val
        result = ''.join('1' if b else '0' for b in val)
        octal = []
        for ndx in range(0, len(result), 3):
            octal.append(bool2oct[result[ndx:ndx+3]])
        while octal and octal[0] == '0':
            del octal[0]
        return octal if octal else '0'

    def show_trace(self, ftype, fname, args):
        """Display the function about to be executed.

        Reconstruct the function about to be executed from:

        ftype  type of function, ACTIVE or NEUTRAL
        fname  name of the function
        args   list of arg strings

        Wait for input before continuing.
        Returns False if execution should continue, True if resetting.
        """

        an_type = '#' if ftype == ACTIVE else '##'
        args_str = ','.join(args)

        prompt = f'TRACE> {an_type}({fname},{args_str})  ? '
        response = input(prompt)

        return response != ''

    def save_environment(self, filename=None):
        """Save the form storage in the named file (default DEFAULT_SAVE)."""

        if filename is None:
            filename = DEFAULT_SAVE

        with open(os.path.expanduser(filename), 'w') as fd:
            fd.write(json.dumps(self.forms, sort_keys=True, indent=2))
    
    def load_environment(self, filename=None):
        """Save the form storage in the named file (default DEFAULT_SAVE)."""

        if filename is None:
            filename = DEFAULT_SAVE

        try:
            with open(os.path.expanduser(filename)) as fd:
                self.forms = json.loads(fd.read())
        except (FileNotFoundError, PermissionError, json.JSONDecodeError) as e:
            if filename != DEFAULT_SAVE:
                self.warn(f"Error opening or reading environment file: {e}")

    ######
    # Trac builtin functions
    #
    # In all these funtions:
    #   aorn  func type, not used here
    #   args  the function arguments
    ######

    def UNDEFINED_builtin(self, aorn, args):
        """Called when an undefined name is executed.
    
        Returns an empty string.
        Should "reset" the system.
        """
    
        self.warn("Undefined string called.")
        return (aorn, '')

    def ps_builtin(self, aorn, args):
        """#(ps,X) - Print string X

        X     is the string to print
              (actually print all args, space separated)

        Prints all args, separated by a space.
        Returns an empty string.
        """

        if args:
            print(' '.join(args))

        return (aorn, '')

    def read_line_into_buffer(self):
        """Read a line from the input stream."""

        # prepare the input prompt, only used on sys.__stdin__ input
        prompt = "Trac64] "
        if args:
            prompt = args[0] + "] "

        if self.commands:
            try:
                line = input()
            except EOFError:
                self.commands = None
                sys.stdin.close()
                sys.stdin = sys.__stdin__
                self.quiet = self.prev_quiet
                line = input(prompt)
            if line and line[-1] == self.meta:
                self.input_buff += line
            else:
                self.input_buff += line + '\n'
        else:
            self.input_buff += input(prompt) + '\n'

    def rs_builtin(self, aorn, args):
        """#(rs) - Read a string up to but not including the meta character

        Excess args are ignored.
        """

        # read next full line complete with meta
        while self.meta not in self.input_buff:
            self.read_line_into_buffer()

        # return string before first meta
        # remove meta and any immediately following newline
        meta_index = self.input_buff.find(self.meta)
        result = self.input_buff[:meta_index]
        self.input_buff = self.input_buff[meta_index+1:]    # strip off first meta
        if self.input_buff and self.input_buff[0] == '\n':
            self.input_buff = self.input_buff[1:]           # and newline

        return (aorn, result)
    
    def rc_builtin(self, aorn, args):
        """#(rc) - Read one character from input, including the current meta

        First arg, if any, is the prompt string.
        Excess args are ignored.
        Newlines are added to the end of each input line.
        """

        prompt = "] "
        if args:
            prompt = args[0] + "] "

        if len(self.input_buff) == 0:
            # need to read more
            self.input_buff += input(prompt) + '\n'

        result = self.input_buff[0]
        self.input_buff = self.input_buff[1:]

        return (aorn, result)
    
    def cm_builtin(self, aorn, args):
        """#(cm,X) - Change the meta character
    
        X     the first character of this string is new mneta

        Do nothing if no args or first arg is an empty string.
        """

        if len(args) >= 1:
            if len(args[0]):
                self.meta = args[0][0]

        return (aorn, '')

    ######
    # Integer arithmetic
    ######

    def ad_builtin(self, aorn, args):
        """#(ad,D1,D2,Z) - Arithmetic add.

        D1, D2  arguments to add
        Z       result if overflow occurs (which can't happen in python)
    
        The argument strings D1 and D2 have a prefix head and a numeric tail.
        The numeric tails are added and the result string is prefixed with
        the head of the D1 string.

        The Z argument may be missing since it cannot be used.
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(ad,...)")
            return (aorn, '')

        (p1, n1) = self.split_prefix_num(args[0])
        (p2, n2) = self.split_prefix_num(args[1])
        return (aorn, f"{p1}{n1+n2}")
    
    def su_builtin(self, aorn, args):
        """#(su,D1,D2,Z) - Arithmetic subtract.

        D1, D2  arguments to subtract
        Z       result if overflow occurs (which can't happen in python)
    
        The argument strings D1 and D2 have a prefix head and a numeric tail.
        The numeric tails are added and the result string is prefixed with
        the head of the D1 string.

        The Z argument may be missing since it cannot be used.
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(su,...)")
            return (aorn, '')

        (p1, n1) = self.split_prefix_num(args[0])
        (p2, n2) = self.split_prefix_num(args[1])
        return (aorn, f"{p1}{n1-n2}")
    
    def ml_builtin(self, aorn, args):
        """#(ml,D1,D2,Z) - Arithmetic multiply.

        D1, D2  arguments to multiply
        Z       result if overflow occurs (which can't happen in python)
    
        The argument strings D1 and D2 have a prefix head and a numeric tail.
        The numeric tails are added and the result string is prefixed with
        the head of the D1 string.

        The Z argument may be missing since it cannot be used.
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(ml,...)")
            return (aorn, '')

        (p1, n1) = self.split_prefix_num(args[0])
        (p2, n2) = self.split_prefix_num(args[1])
        return (aorn, f"{p1}{n1*n2}")
    
    def dv_builtin(self, aorn, args):
        """#(dv,D1,D2,Z) - Arithmetic integer divide.

        D1, D2  arguments to divide
        Z       result if divide by zero occurs
    
        The argument strings D1 and D2 have a prefix head and a numeric tail.
        The numeric tails are added and the result string is prefixed with
        the head of the D1 string.

        The Z argument may be returned on divide by zero.  If that occurs,
        "aorn" is switched to ACTIVE to ensure the result is reevaluated.
        """

        if len(args) < 3:
            self.warn("Bad syntax: not enough args for #(dv,...)")
            return (aorn, '')

        Z = args[2]
        (p1, n1) = self.split_prefix_num(args[0])
        (p2, n2) = self.split_prefix_num(args[1])
        try:
            result = f"{p1}{n1//n2}"
        except ZeroDivisionError:
            result = Z
            aorn = ACTIVE

        return (aorn, result)
    
    ######
    # Boolean operations
    ######

    def bu_builtin(self, aorn, args):
        """#(bu,O1,O2) - Boolean union

        O1, O2  arguments to boolean union
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(bu,O1,O2)")
            return (aorn, '')

        result = ''

        o1 = self.octal_to_bool(args[0])
        o2 = self.octal_to_bool(args[1])

        if len(o1) < len(o2):
            o1 = [False] * (len(o2) - len(o1)) + o1
        elif len(o1) > len(o2):
            o2 = [False] * (len(o1) - len(o2)) + o2

        result = list(map(any, zip(o1, o2)))

        return (aorn, self.bool_to_octal(result))

    def bi_builtin(self, aorn, args):
        """#(bi,O1,O2) - Boolean intersection

        O1, O2  arguments to boolean intersection
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(bi,O1,O2)")
            return (aorn, '')

        o1 = self.octal_to_bool(args[0])
        o2 = self.octal_to_bool(args[1])

        if len(o1) < len(o2):
            del o2[0:len(o2)-len(o1)]
        elif len(o1) > len(o2):
            del o1[0:len(o1)-len(o2)]

        result = list(map(all, zip(o1, o2)))

        return (aorn, self.bool_to_octal(result))

    def bc_builtin(self, aorn, args):
        """#(bc,O1) - Boolean complement

        O1    argument to boolean complement
        """

        if len(args) < 1:
            self.warn("Bad syntax: not enough args for #(bc,O1)")
            return (aorn, '')

        o1 = self.octal_to_bool(args[0])
        result = [not val for val in o1]

        return (aorn, self.bool_to_octal(result))

    def bs_builtin(self, aorn, args):
        """#(bs,D1,O1) - Boolean shift

        D1    number of places to shift, decinal
              shift left if +ve, else right
        O1    octal string to boolean shift
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(bs,D1,O1)")
            return (aorn, '')

        (_, d1) = self.split_prefix_num(args[0])
        o1 = self.octal_to_bool(args[1])

        if d1 < len(o1):
            if d1 > 0:
                # shift left 
                result = o1[d1:] + [False]*d1
            elif d1 < 0:
                # shift right
                result = [False]*d1 + o1[:d1]
            else:
                result = o1
        else:
            result = []

        return (aorn, self.bool_to_octal(result))

    def br_builtin(self, aorn, args):
        """#(br,D1,O1) - Boolean rotate

        D1    number of places to rotate, decimal
              shift left if +ve, else right
        O1    argument to boolean rotate
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(br,D1,O1)")
            return (aorn, '')

        (_, d1) = self.split_prefix_num(args[0])
        o1 = self.octal_to_bool(args[1])

        leno1 = len(o1)
        d1 = d1 % leno1     # only rotate REMAINDER places
        if d1 != 0:
            if d1 > 0:
                # rotate left 
                head = o1[:d1]
                tail = o1[d1:]
            elif d1 < 0:
                # rotate right
                tail = o1[leno1-d1:]
                head = o1[:leno1-d1]

            result = tail + head
        else:
            result = o1

        return (aorn, self.bool_to_octal(result))

    ######
    # Decision functions
    ######

    def eq_builtin(self, aorn, args):
        """#(eq,X1,X2,X3,X4) - String equals

        X1, X2  the strings to compare
        X3      the result if X1 == X2
        X4      the result if X1 != X2
        """

        if len(args) < 4:
            self.warn("Bad syntax: not enough args for #(eq,X1,X2,X3,X4)")
            return (aorn, '')

        if args[0] == args[1]:
            return (aorn, args[2])
        return (aorn, args[3])

    def gr_builtin(self, aorn, args):
        """#(gr,D1,D2,X1,X2) - Numeric greater than

        D1, D2  the integer values (ignoring non-numeric profex) to compare
        X1      the result if D1 > D2
        X2      the result if D1 <= D2
        """

        if len(args) < 4:
            self.warn("Bad syntax: not enough args for #(gr,D1,D2,X1,X2)")
            return (aorn, '')

        (_, d1) = self.split_prefix_num(args[0])
        (_, d2) = self.split_prefix_num(args[1])

        if d1 > d2:
            return (aorn, args[2])
        return (aorn, args[3])

    ######
    # Define and call functions
    ######

    def ds_builtin(self, aorn, args):
        """#(ds,N,X) - Define form in storage

        N       the name of the stored string
        X       the string stored under name 'N'
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(ds,N,X)")
            return (aorn, '')

        self.forms[args[0]] = [[0,0], [args[1]]]

        return (aorn, '')

    def ss_builtin(self, aorn, args):
        """#(ss,N,X1,X2,...) - Segment form in storage

        N       the name of the stored form
        X1,...  the substrings to segment on
        
        The form pointer is reset to the first text character in the new form.
        This isn't defined in the paper, and it's much simpler to reset, so
        that's what we do.  It's not clear if trying to maintain the form
        pointer at the "logical" place in the form is actually useful.
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(ss,N,X1,X2,...)")
            return (aorn, '')

        name = args[0]
        try:
            [fp, form] = self.forms[name]
        except KeyError:
            self.warn(f'Form "{name}" not found.')
            return (aorn, '')

        # now add new gap segments
        for (argnum, argtxt) in enumerate(args[1:]):
            result = []
            for (segnum, segment) in enumerate(form):
                if isinstance(segment, int):
                    result.append(segment)
                    continue
                
                # text segment, look for matches with "argtxt"
                scan = 0
                while (index := segment.find(argtxt, scan)) != -1:
                    # add text before match, if any, to new form list
                    if segment[scan:index]:
                        result.append(segment[scan:index])
                        
                    # now add the new segment gap, move to after the match
                    result.append(argnum)
                    scan = index + len(argtxt)
                    
                # add remaining text of segment, if any
                if segment[scan:]:
                    result.append(segment[scan:])

            form = result
            
        # set FP to first character of first text segment
        fp_segnum = len(form) + 1   # set EOF
        fp_offset = 0
        for (segnum, segment) in enumerate(form):
            if isinstance(segment, str):
                fp_segnum = segnum
                break
            
        self.forms[name] = [[fp_segnum, fp_offset], form]

        return (aorn, '')

    def cl_builtin(self, aorn, args):
        """#(cl,N,X1,X2,...) - Call segmented form from storage

        N       the name of the stored form to retrieve
        X1,...  the substrings to replace numbered segments with
        """

        if len(args) < 1:
            self.warn("Bad syntax: not enough args for #(cl,N,X1,X2,...)")
            return (aorn, '')

        # get the form name and substitution strings
        (name, *args) = args

        try:
            (fp, form) = self.forms[name]
        except KeyError:
            self.warn(f'Form "{name}" not found.')
            return (aorn, '')

        result = []
        for elt in form:
            if isinstance(elt, str):
                result.append(elt)
            else:
                try:
                    replace = args[elt]
                except IndexError:
                    # no arg matching the segment number, use empty string
                    replace = ''
                result.append(replace)

        return (aorn, ''.join(result))

    def cs_builtin(self, aorn, args):
        """#(cs,N,Z) - Call a segment from a form

        N       the name of the stored form
        Z       the string to return if the form is empty
                (ie, form pointer at the end of the form string)
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(cs,N,Z)")
            return (aorn, '')

        N = args[0]
        Z = args[1]

        try:
            (fp, form) = self.forms[N]
        except KeyError:
            self.warn(f'Form "{N}" not found.')
            return (aorn, '')

        (segnum, index) = fp
        if segnum >= len(form):
            # return Z, result forced to ACTIVE
            return (ACTIVE, Z)

        segment = form[segnum]
        result = segment[index:]

        new_fp = self.normalize_fp((segnum+1, 0), form)

        # update the form pointer
        self.forms[N][0] = new_fp
        return (aorn, result)

    def cn_builtin(self, aorn, args):
        """#(cn,N,D,Z) - Call (read) D characters from a form

        N       the name of the stored form
        D       the decimal number of characters to get
        Z       the string to return if the form is empty
                (ie, form pointer at the end of the form string)
        """

        if len(args) < 3:
            self.warn("Bad syntax: not enough args for #(cn,N,D,Z)")
            return (aorn, '')

        N = args[0]
        (_, D) = self.split_prefix_num(args[1])
        Z = args[2]

        try:
            (fp, form) = self.forms[N]
        except KeyError:
            self.warn(f'Form "{N}" not found.')
            return (aorn, '')

        lin_form = self.linearize_form(form)
        lin_fp = self.linearize_fp(fp, form)

        if D > 0:
            result = lin_form[lin_fp:lin_fp+D]
        elif D < 0:
            if lin_fp+D > 0:
                result = lin_form[lin_fp+D:lin_fp]
            else:
                result = lin_form[:lin_fp]

        lin_fp += D

        if not result:
            result = Z

        # reset the FP left or right
        new_fp = self.move_fp(lin_fp, form)

        # update the form pointer, return result
        self.forms[N][0] = new_fp
        return (aorn, result)

    def in_builtin(self, aorn, args):
        """#(in,N,X,Z) - Get string before match of X in Z.

        N       the name of the stored form
        X       the string to search for in the form N
        Z       the string to return if no match is found
        """

        if len(args) < 3:
            self.warn("Bad syntax: not enough args for #(in,N,X,Z)")
            return (aorn, '')

        N = args[0]
        X = args[1]
        Z = args[2]

        try:
            (fp, form) = self.forms[N]
        except KeyError:
            self.warn(f'Form "{N}" not found.')
            return (aorn, '')

        lin_form = self.linearize_form(form)
        lin_fp = self.linearize_fp(fp, form)

        # look for X in lin_form, starting at lin_fp
        ptr = lin_form.find(X, lin_fp)

        if ptr == -1:
            # no find, leave things unchanged, return Z
            result = Z
        else:
            # match result is substring [lin_fp:ptr]
            result = lin_form[lin_fp:ptr]

            # move FP to just after the match, get new FP value
            lin_fp = ptr + len(X)
            new_fp = self.move_fp(lin_fp, form)

            # update the form pointer, return result
            self.forms[N][0] = new_fp

        return (aorn, result)

    def cc_builtin(self, aorn, args):
        """#(cc,N,Z) - Call a single character from a form

        N       the name of the stored form
        Z       the string to return if the form is empty
                (ie, form pointer at the end of the form string)
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(cc,N,Z)")
            return (aorn, '')

        N = args[0]
        Z = args[1]

        try:
            (fp, form) = self.forms[N]
        except KeyError:
            self.warn(f'Form "{N}" not found.')
            return (aorn, '')

        (segnum, index) = fp
        if segnum >= len(form):
            # return Z, resut forced to ACTIVE
            return (ACTIVE, Z)

        segment = form[segnum]
        len_seg  = len(segment)
        result = segment[index]

        new_fp = self.normalize_fp((segnum, index+1), form)

        # update the form pointer, return result
        self.forms[N][0] = new_fp
        return (aorn, result)

    def cr_builtin(self, aorn, args):
        """#(cr,N) - Call restore - rewind form pointer

        N       the name of the stored form
        """

        if len(args) < 1:
            self.warn("Bad syntax: not enough args for #(cr,N)")
            return (aorn, '')

        N = args[0]

        try:
            (fp, form) = self.forms[N]
        except KeyError:
            self.warn(f'Form "{N}" not found.')
            return (aorn, '')

        new_fp = self.normalize_fp((0, 0), form)

        self.forms[N][0] = new_fp

        # return empty result
        return (aorn, '')

    def dd_builtin(self, aorn, args):
        """#(dd,N1,N2,...) - Delete forms with names N1, N2, ...
    
        N1,...  Names of the forms to delete
        """

        for name in args:
            if name in self.forms:
                del self.forms[name]
            else:
                self.warn("Form '{name}' not found.")

        return (aorn, '')

    def da_builtin(self, aorn, args):
        """#(da) - Delete all forms in form storage"""

        self.forms = {}

        return (aorn, '')

    ######
    # External storage management functions
    ######

    def sb_builtin(self, aorn, args):
        """#(sb,N,N1,N2,...) - Store a group of forms in a block

        N         the name used for the external storage
        N1,N2...  the forms to be stored in the block

        After storing the forms N1,N2,... they are all deleted from
        internal storage.  A form N is created with the save filename.
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(sb,N,N1,N2...)")
            return (aorn, '')

        N = args[0]
        filename = f'{N}.trac'

        save_dict = {}
        for name in args[1:]:
            # first, check that form to save exists
            if name not in self.forms:
                # error, form not in store
                self.warn(f"Form named {name} doesn't exist in form storage.")
                return (aorn, '')

            # put form into the save dictionary
            save_dict[name] = self.forms[name]

        # all forms exist and are copied into "save_dict", save as JSON
        try:
            with open(os.path.expanduser(filename), 'w') as fd:
                fd.write(json.dumps(save_dict, sort_keys=True, indent=2))
        except (FileNotFoundError, PermissionError, IsADirectoryError) as e:
            self.warn(f"Error writing to block file: {e}")
            return (aorn, '')

        # now delete the forms
        for name in args[1:]:
            del self.forms[name]

        # finally, create a form named N and save the filename as the form data
        self.forms[N] = [[0,0], [filename]]

        return (aorn, '')

    def fb_builtin(self, aorn, args):
        """#(fb,N) - Fetch a block of saved forms names N

        N       the name of the form containing a file path
        """

        if len(args) < 1:
            self.warn("Bad syntax: not enough args for #(fb,N)")
            return (aorn, '')

        # get the save filename from form N
        N = args[0]
        if N not in self.forms:
            self.warn(f"Can't restore from form '{N}', no such form.")
            return (aorn, '')
        (_, filename) = self.forms[N]   # strip off the form pointer
        filename = filename[0]

        # try to open the file, read forms into a "restore_dict"
        try:
            with open(os.path.expanduser(filename), 'r') as fd:
                restore_dict = json.loads(fd.read())
        except (FileNotFoundError, PermissionError, json.JSONDecodeError) as e:
            self.warn(f"Error opening or reading block file: {e}")
            return (aorn, '')

        # now add the forms read into form storage
        self.forms.update(restore_dict)

        return (aorn, '')

    def eb_builtin(self, aorn, args):
        """#(eb,N) - Erase a block of saved forms named N and form N

        N         the name used for the external storage
        """

        if len(args) < 1:
            self.warn("Bad syntax: not enough args for #(eb,N)")
            return (aorn, '')

        # get the save filename from form N
        N = args[0]
        if N not in self.forms:
            self.warn(f"Can't erase from form '{N}', no such form.")
            return (aorn, '')
        (_, filename) = self.forms[N]   # strip off the form pointer
        filename = filename[0]

        # try to delete the file
        try:
            os.remove(filename)
        except (FileNotFoundError, PermissionError, IsADirectoryError) as e:
            self.warn(f"Error deleting block file: {e}")
            return (aorn, '')

        # now delete the named form (N)
        del self.forms[N]

        return (aorn, '')

    ######
    # Diagnostic functions
    ######

    def ln_builtin(self, aorn, args):
        """#(ln,X) - List names in form storage, prefix each name with X
    
        X     the string to prefix each name in the result string
        """

        prefix = ''
        if args:
            prefix = args[0]

        return (aorn, prefix+prefix.join(self.forms.keys()))

    def pf_builtin(self, aorn, args):
        """#(pf,N1,N2,...) - Print stored forms

        N1 ...  the name(s) of the stored forms to print

        Extends the Trac64 definition.
        If there are no Nx arguments, print ALL stored forms.
        """

        # source of names is either "args" or the forms dictionary
        names_source = args
        if not args:
            names_source = list(self.forms.keys())

        # warn if nothing in form storage
        if len(names_source) == 0:
            self.warn('There are no forms in storage.')
            return (aorn, '')

        # get max length in names
        maxlen = 0
        for name in names_source:
            if len(name) > maxlen:
                maxlen = len(name)

        # now print names and form strings, showing segments
        for name in names_source:
            print(f'"{name}" {" "*(maxlen - len(name))}| "', end='')
            ((segnum, offset), form) = self.forms[name]
            for seg in form:
                if isinstance(seg, str):
                    print(f'{seg}', end='')
                else:
                    print(f"{{{seg}}}", end='')
            print('"')

            # print form pointer
            print(f"{' '*maxlen}   |  ", end='')
            for (i, seg) in enumerate(form):
                if isinstance(seg, str):
                    if i == segnum:
                        print(f"{' '*offset}^", end='')
                        break   # cause finished
                    print(f"{' '*len(seg)}", end='')
                else:
                    print(f"{' '*(len(str(seg))+2)}", end='')
            else:
                print(' ^', end='')
            print()

        return (aorn, '')

    def tn_builtin(self, aorn, args):
        """#(tn) - Turn diagnostic trace ON"""

        self.trace = True

        return (aorn, '')

    def tf_builtin(self, aorn, args):
        """#(tf) - Turn diagnostic trace OFF"""

        self.trace = False

        return (aorn, '')

    ######
    # Extension builtins
    ######

    def wf_builtin(self, aorn, args):
        """#(wf,N,X) - Write string X to file N
    
        N       name of the file to write
        X       string that is written to the file
        """

        if len(args) < 2:
            self.warn("Bad syntax: not enough args for #(wf,N,X)")
            return (aorn, '')

        (N, X, *_) = args

        try:
            with open(os.path.expanduser(N), 'w') as fd:
                fd.write(X)
        except (FileNotFoundError, PermissionError, IsADirectoryError) as e:
            self.warn(f"Error writing text file: {e}")
            # fall through

        return (aorn, '')

    def rf_builtin(self, aorn, args):
        """#(rf,N) - Read text from file N
    
        N       name of the file to read
        """

        if len(args) < 1:
            self.warn("Bad syntax: not enough args for #(rf,N)")
            return (aorn, '')

        N = args[0]
        try:
            with open(os.path.expanduser(N), 'r') as fd:
                result = fd.read()
        except (FileNotFoundError, PermissionError, IsADirectoryError) as e:
            self.warn(f"Error reading text file: {e}")
            return (aorn, '')

        return (aorn, result)

    def exit_builtin(self, aorn, args):
        """#(exit) - Exit the running engine"""

        sys.exit(0)

    ######
    # The Trac state machine
    ######

    def run(self):
        """A little 'stub' method to catch exceptions in self.trac().

        Sets up input history.
        Also load & save the environment.
        """

        if HaveReadline:
            # set up a "readline" history
            # code from <https://docs.python.org/3/library/readline.html>
            histfile = os.path.join(os.path.expanduser("~"), ".trac64_history")
            try:
                readline.read_history_file(histfile)
                # default history len is -1 (infinite), which may grow unruly
                readline.set_history_length(1000)
            except FileNotFoundError:
                pass

            atexit.register(readline.write_history_file, histfile)

        # get any saved environment, if required
        if not self.nosave:
            self.load_environment()

        # run the trac engine
        try:
            self.trac()
            status = 0
        except (EOFError, KeyboardInterrupt):
            print()
            status = 1
        except (SystemExit):
            status = 0
        
        # save the environment, maybe
        if not self.nosave:
            self.save_environment()

        sys.exit(status)

    def trac(self):
        neutral = []                 # the "active" string, a list of characters
        active = list(IDLE_STRING)   # the "active" string, a list of characters
        scan = 0                     # index into "active", scan pointer
        result = ''                  # result string from function
        ch = None                    # the current character being examined
        stack = []                   # stack of "arg_ptr" lists
        
        state = 1                    # state to start the engine
    
        while True:
            match state:
                case 1:
                    try:
                        ch = active[scan]
                    except IndexError:
                        # end of input string
                        state = 14
                        continue
                    state = 2
        
                case 2:
                    if ch == '(':
                        try:
                            next_scan = scan + 1
                            nest_count = 1
                            while True:
                                if active[next_scan] == '(':
                                    nest_count += 1
                                elif active[next_scan] == ')':
                                    nest_count -= 1
                                if nest_count == 0:
                                    neutral.extend(active[scan+1: next_scan])
                                    del active[scan:next_scan+1]
                                    state = 1
                                    break
                                next_scan += 1
                            continue
                        except IndexError:
                            warn("Unclosed parenthesis")
                            state = 14
                            continue
                    state = 3
        
                case 3:
                    if ch.isspace():
                        del active[scan]
                        state = 1
                        continue 
                    state = 4
        
                case 4:
                    if ch == ',':
                        del active[scan]
                        stack[-1].append(len(neutral))
                        state = 1
                        continue
                    state = 5
        
                case 5:
                    if ch == '#':
                        try:
                            if active[scan+1] == '(':
                                # start of "active" function
                                del active[scan:scan+2]
                                stack.append([ACTIVE, len(neutral)])
                                state = 1
                                continue
                        except IndexError:
                            state = 14
                            continue
                    state = 6
        
                case 6:
                    if ch == '#':
                        try:
                            if active[scan+1] == '#':
                                if active[scan+2] == '(':
                                    # start of "neutral" function
                                    del active[scan:scan+3]
                                    stack.append([NEUTRAL, len(neutral)])
                                    state = 1
                                    continue
                        except IndexError:
                            state = 14
                            continue
                    state = 7
        
                case 7:
                    if ch == '#':
                        neutral.append(ch)
                        state = 15
                        continue
                    state = 8
        
                case 8:
                    if ch == ')':
                        del active[scan]
                        try:
                            stack[-1].append(len(neutral))
                        except IndexError:
                            self.warn("Unexpected ')' found.")
                            state = 14
                            continue
    
                        # gather function name and arg strings into a better form
                        (fn_type, *arg_ptrs) = stack[-1]
                        (fname, args) = self.split_fn_args(arg_ptrs, neutral)
                        if not fname:
                            # bad parentheses or empty #()
                            self.warn('Bad parentheses.')
                            state = 14
                            continue
    
                        # if trace is turned on, do it
                        if self.trace:
                            if self.show_trace(fn_type, fname, args):
                                # user wants to reset the engine
                                print('**** RESET ****\n')
                                state = 14
                                continue
                        
                        # find builtin function and execute it
                        func = self.builtins.get(fname, self.UNDEFINED_builtin)
                        (aorn, result) = func(fn_type, args)
    
                        if self.trace:
                            print(f"TRACE> func returns '{result}'")
    
                        # put possibly changed func type back onto stack top args list
                        stack[-1] = (aorn, *arg_ptrs)
                        state = 10
                        continue
                    state = 9
        
                case 9:
                    neutral.append(active[scan])
                    del active[scan]
                    state = 1
        
                case 10:
                    if not result:
                        state = 13
                        continue
                    state = 11
        
                case 11:
                    (fn_type, *arg_ptrs) = stack[-1]
                    if fn_type is ACTIVE:
                        active = active[:scan] + list(result) + active[scan:]
                        state = 13
                        continue
                    state = 12
        
                case 12:
                    if fn_type is NEUTRAL:
                        (_, beg_index, *_) = stack.pop()
                        neutral[beg_index:] = result
                    state = 15
        
                case 13:
                    (_, fn_start, *_) = stack.pop()
                    del neutral[fn_start:]
                    state = 1
        
                case 14:
                    scan = 0                     # index into "active", scan pointer
                    result = ''                  # result string from function
                    ch = None                    # the current character being examined
                    active = list(IDLE_STRING)   # the "active" string, a list of characters
                    neutral = []                 # the "active" string, a list of characters
                    stack = []                   # stack of "arg_ptr" lists
    #                if self.debug:
    #                    input("NEW CYCLE............................ ")
                    state = 1
        
                case 15:
    #                scan += 1
                    state = 1
        
                case _:
                    self.warn('Got bad FSM state: {state}')
                    state = 14

  
############################################################

if __name__ == '__main__':
    import getopt
    import traceback

    # to help the befuddled user
    def usage(msg=None):
        print()
        if msg:
            print(('*'*80 + '\n%s\n' + '*'*80) % msg)
        print("Run Trac64 in a REPL.")
        print()
        print("Usage: trac64.py [-h] [<options>]")
        print()
        print("where  -h         prints this help and stops")
        print()
        print("and <options> may be one or more of:")
        print()
        print("   -c <file>  executes the trac commands in <file> at start")
        print("   -n         inhibit load/save of the environment")
        print("   -q         run quietly, no unnecessary output")
        print("   -t         start excution with trace on")
        print()

    # our own handler for uncaught exceptions
    def excepthook(type, value, tb):
        msg = '\n' + '=' * 80
        msg += '\nUncaught exception:\n'
        msg += ''.join(traceback.format_exception(type, value, tb))
        msg += '=' * 80 + '\n'
        print(msg)

    # plug our handler into the python system
    sys.excepthook = excepthook

    # parse the CLI params
    argv = sys.argv[1:]

    try:
        (opts, args) = getopt.getopt(argv, 'c:hnqt',
                                     ['commands=', 'help',
                                      'noenv', 'quiet', 'trace'])
    except getopt.GetoptError as err:
        usage(err)
        sys.exit(1)

    # default all the possible params
    commands = None
    nosave = False
    quiet = False
    trace = False

    for (opt, param) in opts:
        if opt in ['-c', '--commands']:
            commands = param
        elif opt in ['-h', '--help']:
            usage()
            sys.exit(0)
        elif opt in ['-n', '--nosave']:
            nosave = True
        elif opt in ['-q', '--quiet']:
            quiet = True
        elif opt in ['-t', '--trace']:
            trace = True

    # run the program code
    if not quiet:
        print(f"Trac64 v {TracVersion}")
        if not HaveReadline:
            print("***** Can't use 'readline'. *****")
        print()
    
    trac = TracEngine(commands=commands, nosave=nosave,
                      quiet=quiet, trace=trace)
    trac.run()