#!/bin/env python

"""
Code to implement the algorithm in the paper.

Usage: test.py <imagefile> <outputdatafile>

where <imagefile>      is the input image file
and   <outputdatafile> is the marked output file

Functions:
    debug_plot_H(xdata, title='', alpha_maxH=None, sections=None):
        plots some H data

    create_H_data(Iin)
        creates a 1D H profile array from 2D image data

    open_image(filename)
        opens an image file and returns the 2D image data

    sort_X_by_H(H):
        Returns a sorted descending X list from H data

Status:
    The input images must be 1 bit black/white images. Processing
    to get the images in that form must be done before this code.

    The images *sample2.png* and *sample4.png* are simple test images
    used to test the algorithm.
"""


import sys
import getopt
import traceback
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
    

def debug_plot_H(xdata, title='', alpha_maxH=None, sections=None):
    """Debug: plot an H function.

    xdata       the H data
    title       optional plot title
    alpha_maxH  if specified is the value to draw thew threshold at
    sections    if specified defines the sections to show
                (a list of selected *points*)
    """

    fig, ax = plt.subplots()
    
    # Example data
    ydata = range(len(xdata))

    if alpha_maxH is not None:
        plt.axvline(x=alpha_maxH, color='red', linestyle='--')
        plt.annotate("α*max(H)", (alpha_maxH + 0.3, -len(xdata)//100))

    # annotate graph with contiguous sections
    if sections is not None:
        lsect = []
        sections = list(sections)
        sections.sort()
        start = sections[0]
        prev = start
        for y in sections[1:]:
            if y > prev + 1:
                # end of contiguous segment, draw it
                plt.plot([max(xdata), max(xdata)], [start, prev], '-')
                plt.plot([0, max(xdata)], [start, start], '-')
                plt.plot([0, max(xdata)], [prev, prev], '-')
                start = y
                prev = y
            else:
                prev = y
        plt.plot([max(xdata), max(xdata)], [start, sections[-1]], '-')
        plt.plot([0, max(xdata)], [start, start], '-')
        plt.plot([0, max(xdata)], [sections[-1], sections[-1]], '-')

    # finish the graph
    ax.invert_yaxis() 
    ax.barh(ydata, xdata, 1.0)
    ax.set_xlabel('H value')
    ax.set_ylabel('Row')
    ax.set_title(title)

    plt.show()


def create_H_data(Iin):
    """Creates a numpy 1D array holding H data.

    Iin  numpy 2D array of image data
    """

    # count number of zeros (black pixel) in each row
    H = np.count_nonzero(Iin == 0, axis=1)

    return H

def open_image(filename):
    """Open an image file and return the 2D image data.

    Raises a ValueError exception if the wrong type of image file.
    """

    image = Image.open(filename)

    bands = image.getbands()
    if bands != ('P',):     # 1 bit image?
        # not 1bit colour, abort
        # will handle other image types later, ie, ('R', 'G', 'B')
        raise ValueError(f"Incorrect image type in file {filename}: {bands=}, expected ('P',)")

    # put into numpy array, [row, row, ...]
    return np.array(image)


def sort_X_by_H(H):
    """Returns a sorted descending X list."""

    vx = [(v, x) for (x, v) in enumerate(H)]
    vx.sort(reverse=True)
    return [x for (v, x) in vx]


def main(infilename, outfilename):
    """Just exercise the functions above."""

    # open image file, create H data and plot it
    Iin = open_image(infilename)
    H = create_H_data(Iin)

    alpha = 0.1
    t = 0.6         # guess! not specified in pseudocode
    A = set()
    B = set()
    i = 0
    max_H = max(H)
    X = sort_X_by_H(H)

    # debug, have a look at the results
    debug_plot_H(H, f'H function for {infilename}', alpha_maxH=alpha*max_H)
    debug_plot_H([H[x] for x in X], "X sorted by descending H", alpha_maxH=alpha*max_H)

    X_i = X[0]
    while H[X_i] > alpha * max_H:
        X_i = X[i]
        if X_i not in A:
            talpha = t * H[X_i]
            # now find all ranges R such that...
            x1 = x2 = X_i
            while X[x1-1] <= X_i and H[x1-1] >= talpha:
                x1 -= 1
            while X[x2+1] >= X_i and H[x2+1] >= talpha:
                x2 += 1
            R = set(list(range(x1, x2+1)))

            if not A.intersection(R):
                B = B.union(R)

            A = A.union(R)

        i += 1

    debug_plot_H(H, f'Results for {infilename}', alpha_maxH=alpha*max_H, sections=A)
    print(f'{B=}')

    # now mark areas in image not selected as red
#        To be done
#        can't do this until we accept RGB images

    return 0

##############################################################
# Everything below here is stuff I put in every program.
##############################################################

# to help the befuddled user
def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)


# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    print(msg)


# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'h', ['help'])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

for (opt, param) in opts:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)

if len(args) != 2:
    usage()
    sys.exit(1)

# run the program code
result = main(*args)
sys.exit(result)
