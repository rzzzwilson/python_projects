handwriting
===========

This is some python code that tries to implement the pseudocode in
the PDF file.

Requirements are:

* matplotlib
* numpy
* pillow

I built in a debug function to plot data at various stages to see what
is happening.  That's the *debug_plot_H()* function.  There are optional
parameters to show the *alpha * max(H)* values and selected sections at
the end of the algorithm.

To run this do one of these:

    python test.py sample2.png tmp

    python test.py sample4.png tmp

The *tmp* file is unused but is required.

Status
------

The code works but may not be correct as the selected sections shown in
the last debug plot aren't exactly what I expect.  So maybe more work
to be done!
