from tkinter import *

def click(click_event):
    global prev
    print(f'click: click_event={click_event}')
    prev = click_event

def move(move_event):
    global prev
    print(f'move: move_event={move_event}')
    canvas.create_line(prev.x, prev.y, move_event.x, move_event.y, width=2)
    prev = move_event  # what does this do ?

master = Tk()

canvas = Canvas(master, width=600, height=300, bg='white')
canvas.pack(padx=20, pady=20)

canvas.bind('<Button-1>', click)
canvas.bind('<B1-Motion>', move)

mainloop()
