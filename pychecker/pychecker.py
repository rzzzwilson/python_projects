"""
A problem checker to test a user's code to solve a python problem.

Usage: pychecker [<options>]  [<problem file>]

where <options> may be zero or more of:
    -d    means turn on debug
    -h    print this help and stop
    -s    save the temporary directory from deletion
"""

import sys
import getopt
import os.path
import json
import time
import tempfile
import shutil
import importlib
import filecmp
import traceback
from threading import Thread
from signal import pthread_kill, SIGTSTP, SIGALRM, SIGCHLD
from queue import Queue

from PyQt5.QtWidgets import (QApplication, QWidget, QTextEdit,
                             QGridLayout, QVBoxLayout, QHBoxLayout,
                             QLabel, QPushButton)
from PyQt5.QtCore import (Qt, qDebug, qInstallMessageHandler,
                          QtInfoMsg, QtWarningMsg, QtCriticalMsg, QtFatalMsg)
from PyQt5.QtGui import QFont, QPalette, QColor

from line_numbers import LineNumberWidget


__progname__ = "pychecker"
__version__ = "0.3"

######
# Configuration
######

LOGGING = True
LogFilename = "pychecker.log"
NonCodeFontFamily = "Arial"
CodeFontFamily = "Courier"
NonCodeFontPointSize = 13
CodeFontPointSize = 13

ReportDelim = "-" * 40 + "\n"

ErrorColour = (255, 224, 224)


def count_lines_read(fname, tell):
    """Return number of newlines before position "tell" in file."""

    count = 0
    with open(fname) as fd:
        while fd.tell() < tell:
            ch = fd.read(1)
            if ch == "\n":
                count += 1
    return count

def run_code_in_thread(q, import_name, exception_filename):
    runtime = None
    try:
        start = time.time()
        importlib.import_module(import_name)    # run the user's code
        runtime = time.time() - start
    except Exception as e:
        # get the full traceback
        msg = traceback.format_exc()
        msg_lines = msg.split("\n")
    
        # skip all "File" lines until at "code.py"
        first_line = None
        for (i, line) in enumerate(msg_lines):
            if line.startswith("  File") and "pychecker_" in line:
                first_line = i
                break

        # remove all preceding "File" lines
        if first_line:
            del msg_lines[:first_line]

        for (i, line) in enumerate(msg_lines):
            if line.startswith("  File"):
                (head, body, tail) = line.split('"')
                msg_lines[i] = f'In your code{tail}'

        # now write exception data to the exception report file
        with open(exception_filename, "w") as fd:
            fd.write("There was an exception:\n")
            for line in msg_lines:
                if line:
                    fd.write(line + "\n")

    q.put(runtime)


class MyTextEdit(QTextEdit):
    """Make QTextEdit child class that ignores TAB keypresses."""

    def __init__(self):
        super().__init__()

    def keyPressEvent(self, event):
        if event.text() == "\t":
            pass
        else:
            super().keyPressEvent(event)

#    def keyPressEvent(self, event):
#        if event.key() == Qt.Key_Tab:
#            event = QKeyEvent(QEvent.KeyPress, Qt.Key_Space, Qt.KeyboardModifiers(event.nativeModifiers()), count=4)
#        super().keyPressEvent(event)


class PyChecker(QWidget):
    """Class for the PyChecker widget."""

    def __init__(self, filename, curr_dir, debug, deltemp, parent=None):
        super().__init__(parent)

        self.setWindowTitle(f"{__progname__} {__version__}")
        self.resize(1024, 800)

        self.curr_dir = curr_dir
        self.debug = debug
        self.deltemp = deltemp

        self.error = False
        self.timeout = None
        self.example = None
        self.pre_code = None
        self.post_code = None
        self.changing = False       # True if internally code widget

        grid = QGridLayout()
        self.setLayout(grid)

        problem_label = QLabel("Problem statement")
        problem_label.setFont(QFont(NonCodeFontFamily, NonCodeFontPointSize))
        grid.addWidget(problem_label, 0, 0, 1, 2)
        self.problem_text = QTextEdit()
        self.problem_text.setReadOnly(True)
        self.problem_text.setFont(QFont(NonCodeFontFamily, NonCodeFontPointSize))
        grid.addWidget(self.problem_text, 1, 0, 1, 2)

        result_label = QLabel("Marking Results")
        result_label.setFont(QFont(NonCodeFontFamily, NonCodeFontPointSize))
        grid.addWidget(result_label, 0, 2, 1, 2)
        self.result_text = QTextEdit()
        self.result_text.setAutoFillBackground(True)
#        self.result_text.setTextBackgroundColor(QColor("white"))

        vp = self.result_text.viewport()
        p = vp.palette()
        p.setColor(vp.backgroundRole(), QColor("white"))
        vp.setPalette(p)

        self.result_text.setReadOnly(True)
        self.result_text.setFont(QFont(NonCodeFontFamily, NonCodeFontPointSize))
        grid.addWidget(self.result_text, 1, 2, 1, 2)

        code_label = QLabel("Your Code")
        code_label.setFont(QFont(NonCodeFontFamily, NonCodeFontPointSize))
        grid.addWidget(code_label, 2, 0)


        self.code_text = MyTextEdit()
#        self.code_text = QTextEdit()
        self.code_text.setFont(QFont(CodeFontFamily, CodeFontPointSize))
        self.code_numbers = LineNumberWidget(self.code_text)

        lay = QHBoxLayout()
        lay.addWidget(self.code_numbers)
        lay.addWidget(self.code_text)
        grid.addLayout(lay, 3, 0, 1, 4)

        self.prev_button = QPushButton("Previous")
        self.prev_button.setEnabled(False)
        self.prev_button.clicked.connect(self.prev_set)
        grid.addWidget(self.prev_button, 4, 0, alignment=Qt.AlignLeft)

        self.next_button = QPushButton("Next")
        self.next_button.setEnabled(False)
        self.next_button.clicked.connect(self.next_set)
        grid.addWidget(self.next_button, 4, 1, alignment=Qt.AlignLeft)

        self.mark_button = QPushButton("Mark Code")
        self.mark_button.setEnabled(False)
        self.mark_button.clicked.connect(self.mark_code)
        grid.addWidget(self.mark_button, 4, 3, alignment=Qt.AlignRight)

        # get the specified problem
        self.populate(filename)

        # connect controls to handler
        self.code_text.textChanged.connect(self.code_changed)

    def code_changed(self):
        """Code has changed, check if "mark" button should be active."""

        code = self.code_text.toPlainText()
        self.mark_button.setEnabled(bool(code))

        n = int(self.code_text.document().lineCount())
        self.code_numbers.changeLineCount(n)

    def populate(self, filename):
        """Read data from the file and update widgets."""

        if filename is None:
            return

        with open(filename) as fd:
            data = json.loads(fd.read())

        self.problem = data["problem"]
        self.inputs = data["text_inputs"]
        self.outputs = data["text_outputs"]
        self.timeout = data["timeout"]
        self.example = data["example_code"]
        self.pre_code = data["pre_code"]
        self.post_code = data["post_code"]

        self.problem_text.setText(self.problem)
        self.code_text.setText(self.example)

        # set the line numbers to match text
        n = int(self.code_text.document().lineCount())
        self.code_numbers.changeLineCount(n)

    def prev_set(self):
        print("PREVIOUS")

    def next_set(self):
        print("NEXT")

    def mark_code(self):
        """Mark the code in the "Code" text widget."""

        # clear result text widget
        self.result_text.clear()

        # create temporary directory
        temp_directory = tempfile.mkdtemp(prefix="pychecker_", dir=self.curr_dir)
        temp_directory = os.path.basename(temp_directory)
        qDebug(f"Creating temp directory '{temp_directory}'")

        # create filename paths & put code into file
        code_filename = os.path.join(temp_directory, "code.py")
        input_filename = os.path.join(temp_directory, "input.txt")
        expected_output_filename = os.path.join(temp_directory, "expected_output.txt")
        output_filename = os.path.join(temp_directory, "output.txt")
        exception_filename = os.path.join(temp_directory, "exception.txt")

        # put user code and pre- & post-code into the file to be imported
        with open(code_filename, "w") as code_fd:
            if self.pre_code:
                code_fd.write(self.pre_code + "\n")
            code_fd.write(self.code_text.toPlainText())
            if self.post_code:
                code_fd.write("\n\n" + self.post_code)

        # create an expected output file
        if self.outputs:
            with open(expected_output_filename, "w") as output_fd:
                for line in self.outputs:
                    output_fd.write(f"{line}\n")

        # run marker test
        output_fd = sys.stdout = sys.stderr = open(output_filename, "w")

        import_name = f"{temp_directory}.code"

        q = Queue()

        # run user code in thread, terminate after timeout
        t1 = Thread(target=run_code_in_thread, args=(q, import_name, exception_filename))
        t1.daemon = True
        t1.start()
        t1.join(self.timeout)
        if t1.is_alive():
            pthread_kill(t1.ident, SIGCHLD)     # seems OK
            qDebug("After pthread_kill()")

        runtime = q.get()
        if runtime and runtime > self.timeout:
            qDebug("Took too long...")
            self.report("Your code ran for too long.\n")
            self.report(ReportDelim)

        # close all files
        sys.stdout.flush()
        sys.stdout.close()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

        # flag to show if there was any type of error
        self.error = False

        # check if there was an exception
        if os.path.isfile(exception_filename):
            self.error = True
            with open(exception_filename) as fd:
                for line in fd:
                    self.report(line)
            self.report(ReportDelim)

        # compare expected output with actual output
        if not filecmp.cmp(output_filename, expected_output_filename, shallow=False):
            self.error = True
            self.report(f"The printed output was not as expected.\n")
            self.show_output(output_filename, expected_output_filename)
            self.report(ReportDelim)
        else:
            self.report("The printed output was as expected.\n")

        # if error change result text widget background colour
        vp = self.result_text.viewport()
        p = vp.palette()
        if self.error:
            p.setColor(vp.backgroundRole(), QColor(*ErrorColour))
        else:
            p.setColor(vp.backgroundRole(), QColor("white"))
        vp.setPalette(p)

        # delete temporary directory
        if self.deltemp:
            qDebug(f"Deleting entire directory '{temp_directory}'")
            shutil.rmtree(temp_directory)

    def report(self, msg):
        """Add "msg" to text in the "result" widget."""

        text = self.result_text.toPlainText()
        text += msg
        self.result_text.setPlainText(text)

    def show_output(self, output, expected):
        """Show difference between actual output and expected."""

        self.report(f"Actual output:\n")
        with open(output) as fd:
            lines = fd.readlines()
        if lines:
            for line in lines:
                self.report(f"    {line.strip()}\n")
        else:
            self.report(f"    <no output>\n")

        self.report(f"Expected output:\n")
        with open(expected) as fd:
            lines = fd.readlines()
        if lines:
            for line in lines:
                self.report(f"    {line.strip()}\n")
        else:
            self.report(f"    <no output>\n")

# install our own qDebug handler
# this means any caall to qDebug() writes to "LogFilename" file
def qt_message_handler(mode, context, message):
    """A call to qDebug() ends up here."""

    if not LOGGING:
        return

    filename = os.path.basename(context.file)

    with open(LogFilename, "a") as fd:
        fd.write(f"{filename:>17s}|{context.line:3d}|{context.function:<15s}: {message}\n")


def usage(msg=None):
    """Show the user some help."""

    if msg:
        print(f"{'*'*80}")
        print(msg)
        print(f"{'*'*80}")
    print(__doc__)

def main(filename, debug, save):
    # install out qDebug() handler
    qInstallMessageHandler(qt_message_handler)

    # truncate any existing log file
    with open(LogFilename, "w") as fd:
        pass
   
    # log the start
    if filename:
        qDebug(f"{__progname__} v{__version__}, {debug=}, {save=}, using file '{filename}'")
    else:
        qDebug(f"{__progname__} v{__version__}, {debug=}, {save=}")
    
    curr_dir = os.path.dirname(__file__)
    
    app = QApplication(sys.argv)
    win = PyChecker(filename, curr_dir, debug, save)
    win.show()
    return app.exec_()

#####################################################

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, "dhs", ["debug", "help", "save"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

debug = False       # assume no logging
deltemp = True      # assume delete the temporary directory

for (opt, param) in opts:
    if opt in ["-d", "--debug"]:
        debug = True
    elif opt in ["-h", "--help"]:
        usage()
        sys.exit(0)
    elif opt in ["-s", "--save"]:
        deltemp = False

if len(args) >= 2:
    usage("Too many problemset filenames.")
    sys.exit(10)

filename = None
if len(args) == 1:
    filename = args[0]

# run the program code
result = main(filename, debug, deltemp)
sys.exit(result)
