import json

data = {"type": "inline",
        "problem":      ("Write some code to read two integers using two separate "
                         "calls to input(), add the two numbers and print the result.\n\n"
                         "For example, if your code reads the integers 1 and then 2, "
                         "it will print one line containing 3."),
        "text_inputs":  ["1", "2"],
        "text_outputs": ["3"],
        "timeout":      0.05,
        "example_code": ("# read two integers using input() and print their sum\n"
                         "num1 = ...\n"
                         "# replace this line with the rest of your code\n"
                        ),
        "pre_code":     "",
        "post_code":    "",
       }

with open("problem_0.data", "w") as fd:
    fd.write(json.dumps(data, indent=4))

data = {"type": "function",
        "problem":      ("Given an array of integers nums and an integer target, return "
                         "indices of the two numbers such that they add up to target.\n\n"
                         "You may assume that each input would have exactly one solution, "
                         "and you may not use the same element twice.\n\n"
                         "You can return the answer in any order.\n\n"
                         "Example 1:\n"
                         "    Input: nums = [2,7,11,15], target = 9\n"
                         "    Output: [0,1]\n"
                         "    Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].\n\n"
                         "Example 2:\n"
                         "    Input: nums = [3,2,4], target = 6\n"
                         "    Output: [1,2]\n\n"
                         "Example 3:\n"
                         "    Input: nums = [3,3], target = 6\n"
                         "    Output: [0,1]\n\n"
                         "Constraints:\n"
                         "    2 <= nums.length <= 104\n"
                         "    -109 <= nums[i] <= 109\n"
                         "    -109 <= target <= 109\n\n"
                         "Only one valid answer exists."
                        ),
        "text_inputs":  None,
        "timeout":      0.05,
        "example_code": ("def twoSum(nums, target):\n"
                         '    """Return indices of numbers that add up to "target"."""\n\n'
                        ),
        "pre_code":     "",
        "post_code":    ("nums = list(range(10))\n"
                         "target = 3\n"
                         "result = twoSum(nums, target)\n"
                         "print(result)\n"
                        ),
        "text_outputs": ["[1, 2]"],
       }

with open("problem_1.data", "w") as fd:
    fd.write(json.dumps(data, indent=4))


