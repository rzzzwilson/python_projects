# pychecker

The code here is an attempt to do something like what the online
code checkers in places like leetcode.com do.  But as a desktop
program.

The things a checker should do:

* Show the problem and any test cases
* Allow the user to enter and edit code
* Show *as much as possible* what the user code did and why it failed
* Fail a test case if too much time was used
* Be able to generate test data if it would be too big to store

## App behaviour

The test cases should be bundled up in some form (\*.zip?).  The user
should be able to use third-party problem sets if they are in the correct
form.

The user should be able to load and save test code.
