Introduction
============

I wrote the original of this program a *very* long time ago (1977).  I was
studying at Sydney University in the Basser Department of Computer Science.  One
of the lecturers had a puzzle that consisted of a chessboard (all one colour)
that was cut into a small number of irregular contiguous shapes consisting of
board squares glued together.  The puzzle was to place all the pieces such that
the original board was reconstructed.  A challenge was laid down - solve the
puzzle using a computer.

I did so, initially using Pascal on a CDC Cyber mainframe to get the basic idea
tested, which was exhaustive search with backtrack.  My student account on that
machine was just about to run out but I had learned that the accounting test
was applied only at the **beginning** of a session.  So I started my first real
test of the program and waited, hoping it would work as I only had one shot on
the machine.  After about 20 minutes the program printed a first solution!

Next I rewrote the program in BCPL and ran it on a Burroughs 1700.  Some effort
was required to make the program fit on that machine but it eventually worked
well.  I believe it was called *puzz* on the Burroughs.

I had in the back of my mind the idea that it would be possible to write a
similar program that would accept a description of any board and the puzzle
pieces and compute solutions (if any).  I tried to write this in C some years
later but could never get it debugged.

Then python came along.  I always try to learn new languages by reimplementing
various programs I had written in the past in the new language, so my first
attempt at anything other than a toy program was **solver**.  It went together
surprisingly quickly and much more easily than the equivalent C program.  It
also needed far less debugging. This version just printed solutions to the
console.

I wanted to *see* the algorithm doing its backtracking, so I wrote a new version
that displayed the board to solve and the current possible solution pieces on
the board, using Tkinter.  The hard part was generating representations of the
pieces from the input data file.

This was solved by pre-generating partial tiles that showed each quarter portion
of a tile with all combinations of possible neighbour quarter tiles in a puzzle
piece.  These were put into the **tiles** subdirectory.  The program uses those
quarter tiles when constructing the picture of each puzzle piece.  You can see 
the result of doing this in this snapshot of the program in action on test.dat:

.. image:: solver_test_dat.png

It worked on my development platform, which was Linux.  I had given no thought
to running under Windows, but I thought I might just install python there and
see what happened.  Unsurprisingly, it didn't work properly.  Running it without
graphics did work, but the graphics option resulted in a blank screen.  I forget
the reason why, but a little searching found that I needed one more
initialization statement and then the graphics worked under Windows.

That experience sold me on python!

Usage
=====

The program is executed so:

::

    solver [-g] <datafile>

where the **-g** option turns on the graphical display.  Note that the program
is much slower with this option.  In addition to writing each solution to the
console output, each solution is saved as an encapsulated postscript file to
the current directory.

The **<datafile>** is the data file describing the puzzle.

A simple run with the graphical display would be:

::

    solver -g test.dat

Data file format
================

The data file describing the puzzle to solve is a simple text file.  For
example, **test.dat** contains:

::

    3
    3
    XX
    X  
    
    X
    XX
     X
    
    XX 

The first two lines of the file contain the board width and height in squares.
The remaining lines describe the piece shapes, separated by blank lines.  The
example above has three pieces.  The pieces do not need to be in any particular
orientation as long as at least one square of the piece is in column 1.
