"""
A program to solve the challenge laid down in this video:

    https://www.youtube.com/watch?v=G_UYXzGuqvM

The challenge is:

    Write a python program which uses backtracking
    to solve the Klotski puzzle.

Usage: klotski.py [-d] [-m] [-h]

where -d turns on debug
and   -m turns off the "More?" prompt after results
and   -h prints this text and stops.
"""

import sys
import copy
import getopt

# describe the initial board position
# 0 means square is empty
Board = [[1, 10, 10, 2],
         [1, 10, 10, 2],
         [3,  4,  4, 5],
         [3,  6,  7, 5],
         [8,  0,  0, 9]
        ]

# all possible moves of a piece by one place
#            Up      Down   Right  Left   
AllMoves = ((0,-1), (0,1), (1,0), (-1,0))

# map moves to the "undo" move
# key:value is <original move>:<"undo" move>
UndoMoves = {(0,-1): (0,1),
             (0,1):  (0,-1),
             (1,0):  (-1,0),
             (-1,0): (1,0)
            }

# given the top-left unit square of any piece, get all unit squares
# in a list of (dx, dy)
PieceShape = { 1: [(0, 0), (0, 1)],
               2: [(0, 0), (0, 1)],
               3: [(0, 0), (0, 1)],
               4: [(0, 0), (1, 0)],
               5: [(0, 0), (0, 1)],
               6: [(0, 0)],
               7: [(0, 0)],
               8: [(0, 0)],
               9: [(0, 0)],
              10: [(0, 0), (1, 0), (0, 1), (1, 1)],
             }

# dict to replace shapes with the "canonical" shape number
# used to create signature string
# key:value is <piece#>:<canonical#>
CanonicalShapes = {2: 1, 3: 1, 5: 1, 7: 6, 8: 6, 9: 6}

# list of positions already seen
# this allows us to check for previous positions, which we avoid
SeenPositions = []

# solution counter
SolnNum = 0


def debug(msg):
    if opt_debug:
        print(msg)

def print_board():
    """Print board nicely."""

    print('+-----.-----.-----.-----+')

    for y in range(5):
        line = '|'
        tween = '.'
        for x in range(4):
            # fill in one square of piece
            if Board[y][x]:
                line += f'{Board[y][x]:1X}' * 5
            else:
                line += ' ' * 5

            # do space between columns
            if x+1 < 4:
                if Board[y][x] == Board[y][x+1]:
                    if Board[y][x]:
                        line += f'{Board[y][x]:1X}'
                    else:
                        line += ' '
                else:
                    line += ' '
            
            # do space between rows
            if y+1 < 5:
                if Board[y][x] == Board[y+1][x]:
                    if Board[y][x]:
                        tween += f'{Board[y][x]:1X}' * 5
                    else:
                        tween += ' ' * 5
                else:
                    tween += ' ' * 5
            if x+1 < 4 and y+1 < 5:
                if Board[y][x] == Board[y+1][x+1]:
                    if Board[y][x]:
                        tween += f'{Board[y][x]:1X}'
                    else:
                        tween += ' ' 
                else:
                    tween += ' '
        print(line+'|')
        print(line+'|')
        print(line+'|')
        if y+1 < 5:
             print(tween+'.')
    print('+-----+           +-----+')

def move_allowed(piece, x, y, delta):
    """Returns True if "piece" at (x, y) can move "delta".

    "delta" is a tuple of (dx, dy).
    """

    debug(f'move_allowed: {piece=}, {x=}, {y=}, {delta=}')

    (delta_x, delta_y) = delta

    for (dx, dy) in PieceShape[piece]:
        new_y = y + dy + delta_y
        new_x = x + dx + delta_x
        
        if not (0 <= new_y < 5) or not (0 <= new_x < 4):
            debug(f"move_allowed: {piece=}, {x=}, {y=} can't move {delta=}")
            return False
        if Board[new_y][new_x] not in (0, piece):
            debug(f"move_allowed: {piece=}, {x=}, {y=} can't move {delta=}")
            return False

    debug(f"move_allowed: {piece=}, {x=}, {y=} can move {delta=}")
    return True

def get_moves():
    """Given current Board, work out all possible moves.

    Returns a list of tuples: (piece, place, (dx, dy))
    where "piece" is piece name, "place" is top-left position (x, y),
    and (dx, dy) is possible move.
    """

    moves = []
    pieces_done = []

    for y in range(5):
        for x in range(4):
            piece = Board[y][x]
            if piece and piece not in pieces_done:  # not space and new piece?
                pieces_done.append(piece)

                # check for move in different directions
                for dirn in AllMoves:
                    if move_allowed(piece, x, y, dirn):
                        moves.append((piece, (x, y), dirn))
    return moves

def move_piece(piece, place, dirn):
    """Move the named piece at place in the given direction.

    Returns the new position of the piece.
    """

    (place_x, place_y) = place
    (dirn_x, dirn_y) = dirn

    # fill original place with "spaces"
    for (px, py) in PieceShape[piece]:
        Board[place_y+py][place_x+px] = 0

    # fill new place with "piece"
    for (px, py) in PieceShape[piece]:
        Board[place_y+py+dirn_y][place_x+px+dirn_x] = piece

    new_place = (place_x+dirn_x, place_y+dirn_y)
    debug(f'move_piece: {place=}, {dirn=}, {new_place=}')
    return new_place

def already_seen():
    """Decide of we have seen this position before.

    Create a "signature" string and save in SeenPositions list.
    Return True if position seen before, else False.
    """

    debug('already_seen: entered')

    # create signature string
    # map all same shape pieces into a "canonical" piece
    sig = ""
    for row in Board:
        for val in row:
#            sig += f'{val:1X}'
            sig += f'{CanonicalShapes.get(val, val):1X}'

    debug(f'already_seen: {sig=}, {len(SeenPositions)=}')

    if sig in SeenPositions:
        debug('already_seen: returning True')
        return True

    # save this position, it has now been seen
    SeenPositions.append(sig)
    debug('already_seen: returning False')
    return False

def solve(depth=1):
    """Print each solution and solution number."""

    global Board, SolnNum

    # if board already seen, do nothing & just return
    if already_seen():
        return

    # if we have a solution, handle it & return
    if Board[4][1] == 10 and Board[4][2] == 10:
        SolnNum += 1
        print(f'\nSolution {SolnNum} ({depth=}):')
        print_board()
        if opt_more:
            input('More? ')
        return

    # try all possible moves from this position
    # instead of copying Board and restoring after recursive call,
    # we remember the "undo" move and apply it after recursive call returns
    for (piece, place, dirn) in get_moves():
        undo = UndoMoves[dirn]
        new_place = move_piece(piece, place, dirn)
        solve(depth+1)
        move_piece(piece, new_place, undo)


# to help the befuddled user
def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'dmh', ['debug', 'more', 'help'])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

# default the global option flags
opt_debug = False           # no logging
opt_more = True             # ask after a solution

for (opt, param) in opts:
    if opt in ['-d', '--debug']:
        opt_debug = True
    if opt in ['-m', '--more']:
        opt_more = False
    elif opt in ['-h', '--help']:
        usage()
        sys.exit(0)

# now start the search
# we expect depth to exceed 1000 deep
sys.setrecursionlimit(10000)

try:
    solve()
except (KeyboardInterrupt, BrokenPipeError):
    print()

print(f'Found {SolnNum} solutions.')
