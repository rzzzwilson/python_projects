Klotski
=======

When watching 
`this video on solving sudoku <https://www.youtube.com/watch?v=G_UYXzGuqvM>`_
with recursion and backtracking the challenge was laid down: 
"write a python program which uses backtracking to solve
`the Klotski puzzle <https://en.wikipedia.org/wiki/Klotski>`_".

This is my attempt at the solution.  Initially the solutions(s) will be printed
on the console but the final aim is to create a graphical UI to *see* the
backtracking take place.

The first solution is depth-first search.  Try another version that uses other search algorithms.
