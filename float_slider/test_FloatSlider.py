#!/usr/bin/env python

"""
"""

import sys
try:
    from PyQt5.QtWidgets import QWidget, QApplication, QHBoxLayout
except ImportError: 
    from PyQt5.QtWidgets import QWidget, QApplication, QHBoxLayout

from floatslider import FloatSlider, Communicate

#    name            name added to event on value change
#    limits          (min, max, default) real values for slider
#    internal2real   function to convert internal integer to real value
#                        (real_value, display) = internal2real(internal_value)
#                        where 'display' is the string to display in
#                        the textbox
#    real2internal   function to convert real value to internal integer
#                        internal_value = real2internal(real_value)
#    slidersize      slider size tuple: (width, height) in pixels
#    slidertextsize  slider value text size tuple: (width, height) in pixels
#    border          LEFT+RIGHT border on value QLabel

def internal2real(int_value):
    return (int_value/100, f"{int_value/100:.2f}")

class TestWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.c = Communicate()
        self.wid = FloatSlider('test', (0, 101, 50), internal2real, None, (100, 30), (50, 30), 5)
        self.c.update[int].connect(self.wid.setValue)

        hbox = QHBoxLayout()
        hbox.addWidget(self.wid)
        self.setLayout(hbox)

        self.setGeometry(300, 300, 390, 210)
        self.setWindowTitle('FloatSlider widget')
        self.show()

    def changeValue(self, value):
        self.c.update.emit(value)
        self.wid.repaint()

app = QApplication(sys.argv)
ex = TestWidget()
sys.exit(app.exec_())
