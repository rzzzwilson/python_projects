#!/usr/bin/env python

"""
A 'floatslider' widget.

As the PyQt QSlider widget works only with integers, we need to make a slider
that appears to use floats.  Combine the QSlider with a QLabel to display
the slider value and the slider itself:

    +--------------------------------------------------------------+
    |   +--------+                                                 |
    |   |        |    -------^----------------------------------   |
    |   | Qlabel |    ------(*)---------------------------------   |
    |   |        |    -------v----------------------------------   |
    |   +--------+                                                 |
    +--------------------------------------------------------------+

The user will use real external values which are mapped to integer internal
values for the actual underlying slider.

Upon instantiation supply:
    name            name added to event on value change
    limits          (min, max, default) real values for slider
    internal2real   function to convert internal integer to real value
                        (real_value, display) = internal2real(internal_value)
                        where 'display' is the string to display in
                        the textbox
    real2internal   function to convert real value to internal integer
                        internal_value = real2internal(real_value)
    slidersize      slider size tuple: (width, height) in pixels
    slidertextsize  slider value text size tuple: (width, height) in pixels
    border          LEFT+RIGHT border on value QLabel

Methods are:
    .setValue(value)
        Set the widget real value

    value = .getValue()
        Get the widget real value

Events:
    EVT_FLOATSLIDER
        Triggered on widget value change:
            event.name   identifying name of the widget
            event.value  real value of the widget
"""

import sys
try:
    from PyQt6.QtWidgets import QWidget, QSlider, QLabel, QHBoxLayout
    from PyQt6.QtCore import QObject, Qt, pyqtSignal
except ImportError:
    from PyQt5.QtWidgets import QWidget, QSlider, QLabel, QHBoxLayout
    from PyQt5.QtCore import QObject, Qt, pyqtSignal


class Communicate(QObject):
    update = pyqtSignal(int)


class FloatSlider(QWidget):
    def __init__(self, name, limits, internal2real, real2internal,
                       slidersize, slidertextsize, border):
        super().__init__()

        self.name = name
        (self.min, self.max, self.value) = limits
        self.internal2real = internal2real
        self.real2internal = real2internal
        (self.s_width, self.s_height) = slidersize
        (self.stext_width, self.stext_height) = slidertextsize
        self.border = border

        self.initUI()

    def initUI(self):
        hbox = QHBoxLayout()

        self.label = QLabel("test", self)
        self.label.setStyleSheet("border: 1px solid gray;")
        self.label.setFixedWidth(self.stext_width)
        self.label.setFixedHeight(self.stext_height)
        self.label.setAlignment(Qt.AlignCenter)
        hbox.addWidget(self.label)

        self.slider = QSlider(Qt.Horizontal, self)
        self.slider.setFixedWidth(self.s_width)
        self.slider.setFixedHeight(self.s_height)
        self.slider.valueChanged.connect(self.value_change)
        hbox.addWidget(self.slider)

        self.setLayout(hbox)

    def value_change(self):
        (real_value, display) = self.internal2real(self.slider.value())
        self.label.setText(f"{display}")

    def setValue(self, value):
        self.value = value

    def getValue(self):
        return self.value
