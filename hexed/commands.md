The *hexed* commands are loosely based on *vi*.

| command     |done| description |
|-------------|----|-------------|
| :w          | Y | write data to the assumed file, "dirty" flag cleared |
| :w \<file\> | Y | write data to \<file\>, assumed file unchanged, "dirty" flag unchanged |
| :wq         | Y | write data to the assumed file and quit |
| :q!         | Y | quit even if there is unsaved data |
| :r \<file\> | Y | read data from \<file\> and insert after cursor |
| :\<N\>      | Y | go to row with starting offset \<N\>, \<N\> can be a hex or decimal number, first row is offset 0 |
| :$          | Y | go to last row |
|             |   |                |
| :undo       |   | undo the previous edit |
| :redo       |   | redo the previous undone edit |
| \<N\>u      | Y | undo the previous edit, \<N\> assumed to be 1 if not supplied |
| \<N\>^R     | Y | redo the previous undone edit, \<N\> assumed to be 1 if not supplied |
|             |   |                |
| ^X          | Y | toggle HEX/ASCII edit mode |
| i           | Y | enter insert mode, new data inserted to left of cursor, ESCAPE terminates |
| a           |   | enter append mode, new data inserted to right of cursor, ESCAPE terminates |
| R           | Y | enter replace mode, ESCAPE terminates |
| \<N\>x      | Y | delete data under the cursor, \<N\> assumed to be 1 if not supplied |
| \<N\>X      | Y | delete data left of the cursor, \<N\> assumed to be 1 if not supplied |
| \<N\>dd     |   | delete entire row containing the cursor, \<N\> assumed to be 1 if not supplied |
|             |   | 		      |
| \<N\>\<arrow\> | Y | move cursor one position left/up/down/right, \<N\> assumed to be 1 if missing |
| \<N\>h      | Y | move cursor one position left, \<N\> assumed to be 1 if missing |
| \<N\>j      | Y | move cursor one position down, \<N\> assumed to be 1 if missing |
| \<N\>k      | Y | move cursor one position up, \<N\> assumed to be 1 if missing |
| \<N\>l      | Y | move cursor one position right, \<N\> assumed to be 1 if missing |
| \<N\>^F     | Y | move down one screen, \<N\> assumed to be 1 if missing |
| \<N\>^B     | Y | move up one screen, \<N\> assumed to be 1 if missing |
| \<N\>^D     | Y | move down one half-screen, \<N\> assumed to be 1 if missing |
| \<N\>^U     | Y | move up one half-screen, \<N\> assumed to be 1 if missing |
| 0           | Y | move to beginning of row |
| $           | Y | move to end of row |
|             |   |                |
| \<N\>/\<data\> |   | search forward for the \<data\>, \<N\> assumed to be 1 if missing, pressing ENTER starts the search |
| \<N\>/      |   | repeat forward search using previous search \<data\> |
| \<N\>?\<data\> |   | search backward for the \<data\>, \<N\> assumed to be 1 if missing, pressing ENTER starts the search |
| \<N\>?      |   | repeat backward search using previous search \<data\> |
