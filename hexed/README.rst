hexed
=====

A file hex editor that can delete and insert data as well as 
simply overwrite data.

Moded, either hex or ASCII changes, replace/insert/delete.

Try to use a subset of *vim* commands.

Will work with large files without loading all file data into memory.

Commands
--------

The file *commands.md* contains notes on what commands will be implemented and
whether they have been completed or not.

Status
======

Currently working and can read and write a file.  Can overwrite HEX/ASCII
data.

Working on changing commands to be more like Vim.  Something like `:w` to write
a file, `:wq` to write and quit,  Use `x` to delete a byte, `i` to insert, `R`
to replace, and so on.
