#!/usr/bin/env python

"""
An Undo/Redo class.

The basic data structures are:
    .undo_list  list of undo objects
    .redo_list  list of redo objects

On a .save() call, append new state goes to undo list.
Also truncate the redo_list.

On .undo() call, pop youngest state from undo_list, return it.

On .redo() call, pop next redo from redo_list, return it.

On .flush() call, initialize the undo/redo system to "empty".

Note that we use deepcopy of states, as we cannot use originals, which will
change.

Methods:
    obj = UndoRedo()
        Create undo/redo object, initially empty
    obj.save(state1)
        Save 'state1' to system
    obj.refresh(state1)
        Change latest save to reflect 'state' - used to 'telescope' changes
    state = obj.undo()
        Pop UNDO state, return None if no UNDO possible
    state = obj.redo()
        Pop REDO state, return None if no REDO possible
    bool = obj.can_undo()
        Return True if UNDO is possible
    bool = obj.can_redo()
        Return True if REDO is possible
    state = obj.peek_undo()
        Get the state we would UNDO to, disturb nothing, None if no UNDO
    state = obj.peek_redo()
        Get the state we would REDO to, disturb nothing, None if no REDO
    str = obj.dump()
        Returns a debug string of module state
    state = obj.pop()
        Remove and return latest undo snapshot
        Don't allow redo
    obj.flush()
        Empty the undo/redo system.

The .refresh() method above is used to 'telescope' similar changes into one
undo object.  An example is single-character changes to a name field.  We don't
really want to have many undos, one for each character.  So we telescope the
changes into one undo object by saving the first change character as an undo
and replacing the undo with the new state on each successive character change.

That is, in example code, we have something like the following code in
the handler for BT CHANGE code:

    if self.last_event_type == CHANGE:
        self.undoredo.refresh(state)
    else:
        self.undoredo(save(state))
    self.last_event_type = CHANGE
"""

import copy


class UndoRedo:
    """A class that encapsulates the undo/redo mechanism."""

    def __init__(self, logger):
        """Instantiate the undo/redo object.

        state  current state object for undo/redo start
        """

        self.logger = logger                    # remember the logging function
        self.flush()                            # create empty undo/redo lists

    def flush(self):
        """Empty the undo/redo lists."""

        self.undo_list = []                     # list of undo states
        self.redo_list = []                     # list of redo states

    def save(self, state):
        """Save a new current state.

        state  the new application state
        """

        copy_state = copy.deepcopy(state)
        self.undo_list.append(copy_state)
        self.redo_list = []                     # drop any pending redos

    def refresh(self, state):
        """Swap latest current undo with new state.

        state  the new application state

        We need to do this if we are 'telescoping' in calling code.  We
        telescope as we don't want typing in 3 letters to a textbox (for
        example) to be seen as three separate undo steps, just one.
        """

        copy_state = copy.deepcopy(state) 
        self.undo_list[-1] = copy_state

    def undo(self):
        """Do an undo.

        Return any pending undo, or None if undo not possible.
        """

        if not self.undo_list:
            return None

        state = self.undo_list.pop()
        self.redo_list.append(state)

        return state

    def pop(self):
        """Just remove latest undo snapshot."""

        return self.undo_list.pop()

    def redo(self):
        """Do a redo.

        Return any pending redo, or None if redo not possible.
        """

        if not self.redo_list:
            return None

        state = self.redo_list.pop()
        self.undo_list.append(copy.deepcopy(state))

        return state

    def can_undo(self):
        """Can we do an Undo?"""

        return self.undo_list

    def can_redo(self):
        """Can we do an Undo?"""

        return self.redo_list

    def peek_undo(self):
        """Get state that we would UNDO to, None if no UNDO."""

        try:
            return self.undo_list[-1]
        except IndexError:
            return None

    def peek_redo(self):
        """Get state that we would REDO to, None if no REDO."""

        try:
            return self.redo_list[-1]
        except IndexError:
            return None

    def dump(self):
        """Return system state as a string."""

        self.logger("UndoRedo object:")

        if self.undo_list:
            prefix = "   Undo"
            for (i, s) in enumerate(self.undo_list):
                self.logger(f"{prefix} {i:2d}: {s}")
                prefix = " " * len(prefix)
        else:
            self.logger("   Undo   : <none>")

        if self.redo_list:
            prefix = "   Redo"
            for (i, s) in enumerate(self.redo_list):
                self.logger(f"{prefix} {i:2d}: {s}")
                prefix = " " * len(prefix)
        else:
            self.logger("   Redo   : <none>")
