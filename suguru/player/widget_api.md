The API to the Suguru player widget is:

## Widget creation

**from suguru import Suguru**

**widget = Suguru(parent=None, maxsize=MaxSize)**

    Creates the Suguru widget.

        maxsize - stes the maximum dimension size of the puzzle

## Project file operations

**widget.new_file(save=True)**

    Creates a new, empty project.

        save - if True the existing file is saved to the undo/redo mechanism
               (save=False used in intial creation)

**widget.open_file(filename)**

    Opens and reads data from "filename".

**widget.save_file(filename)**

    Saves project data in "filename".

**widget.debug()**

    Prints some debug information to the console.

## Widget attributes

**widget.dirty**

    True if the widget contains a unsaved data.

**widget.project_name**

    If not None contains the short name of the project.

**widget.version**

    A string value that contains the widget version.

