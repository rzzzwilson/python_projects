# Suguru player

This is a PyQt5 GUI program that lets a player try to solve a
Suguru puzzle.

The heart of the code is a custom widget similar to that in
the *../editor* subdirectory.

There may be some good in merging the *player* and *editor*
custom widgets into one general purpose widget.

##player custom widget

The *player* custom widget will be similar to the *editor* widget:

* ability to save and load puzzle data
* right-mouse click brings up a context menu to insert/remove a *value*
  into a cell
* shift-right-mouse click brings up a context menu to insert/remove one
  or more *guesses* into a cell
* key presses of numbers (vanilla and shift) will place/remove *values*
  and *guesses*
* context menus will display only legal choices
* key presses will only have effect on legal choices
* for key presses to work there must be a focussed cell, want to make
  which cell has the focus obvious

The widget itself will handle menus, focus and key presses.  Everything else,
save/load, undo/redo, help, etc, will be handled by the app.
