"""
A custom widget for solving Suguru puzzles.

High-level operations:

* load/save/print puzzles
* right mouse-click to edit cell value
* shift right mouse-click to edit cell guess value(s)
* keyboard equivalents to above value/guess setting/resetting
  (needs some way of showing "focus")
* an undo/redo mechanism

"""

import sys
import os.path
import copy
import math
from pprint import pformat

from PyQt5.QtCore import Qt, QPoint, QObject, QRect, pyqtSignal
from PyQt5.QtGui import (QPainter, QColor, QPen, QPainterPath, QKeyEvent, QFont,
                         QFontMetrics)
from PyQt5.QtWidgets import (QWidget, QMenu, QMessageBox, QVBoxLayout, QLabel,
                             QCheckBox, QDialog, QFormLayout, QPushButton)

from undo_redo import UndoRedo
import logger
log = logger.Log('suguru.log', logger.Log.DEBUG)


_version = "1.0"


###############################################################################
# the dialog to edit the cell guesses
###############################################################################

class EditGuess(QDialog):
    """Dialog to show possibilities with state.

    guesses  a list of tuples: (guess, True/False)
             the "guess" is a guess value, True if already set

    Returns a list of selected guesses.
    """

    def __init__(self, parent=None, guesses=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        self.setWindowTitle("Edit cell guesses")
#        self.setMinimumWidth(300)
#        self.setMinimumHeight(300)

        layout = QFormLayout()
        self.setLayout(layout)

        self.ckboxes = []
        for (val, checked) in sorted(guesses):
            chkbox = QCheckBox(text=str(val))
            chkbox.setChecked(checked)
            layout.addWidget(chkbox)
            layout.addRow(chkbox)

            self.ckboxes.append((val, chkbox))

        # now for the "OK" and "Cancel buttons
        btn_ok = QPushButton("OK", parent)
        btn_ok.clicked.connect(super().accept)
        layout.addWidget(btn_ok)
        layout.addRow(btn_ok)

        btn_cancel = QPushButton("Cancel", parent)
        btn_cancel.clicked.connect(super().reject)
        layout.addWidget(btn_cancel)
        layout.addRow(btn_cancel)

    def get_state(self):
        """Return the final checkbox settings.

        Returns a list of tuples: (value, True/False).
        """

        return [(value, ckbox.isChecked()) for (value, ckbox) in self.ckboxes]


###########################################################
# The custom widget displaying the puzzle
###########################################################

class Communicate(QObject):
    update_undoredo = pyqtSignal()


class Suguru(QWidget):

    # definitions for the Suguru widget
    GridColour = Qt.black                       # colour of the canvas grid lines
    GroupOutlineColour = QColor(255, 63, 63)    # group outline
    WidgetOutlineColour = Qt.blue               # the colour of outline around widget
    CanvasBG = QColor(242, 242, 242)            # the canvas background colour
    FocusBG = QColor(200, 255, 200)             # the focus cell background colour
    GroupBG = Qt.white                          # background of group cells
    ImmutableColour = Qt.blue                   # colour of immutable values
    TextColour = Qt.black                       # colour of value text
    GuessColour = Qt.red                        # colour of guess numbers
    MaxSize = 20                                # max dimension

    def __init__(self, parent=None, x_size=10, y_size=10, *args, **kwargs):
        super().__init__(*args, **kwargs)   # initialize the underlying QWidget

        self.version = _version             # make version visible

        # start a default project
        self.init_vars(x_size, y_size)
        self.maxsize = Suguru.MaxSize

        self.font_size = 10
        self.font = QFont('Terminus', self.font_size, QFont.Bold)

        self.gfont_size = 4
        self.gfont = QFont('Terminus', self.font_size, QFont.Bold)

        self.editor = UndoRedo()            # start the undo/redo system
        self.c = Communicate()

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Suguru.CanvasBG)
        self.setPalette(p)

        # start mouse tracking
        self.setMouseTracking(True)

        # dictionary for focus movements on arrow key
        self.key_deltas = {Qt.Key_Up: (0, -1),
                           Qt.Key_Down: (0, 1),
                           Qt.Key_Left: (-1, 0),
                           Qt.Key_Right: (1, 0)
                          }

    def init_vars(self, x_size, y_size):
        """Initialize all state variables.

        x_size - X size of the new puzzle
        y_size - Y size of the new puzzle
        """

        self.x_size = x_size            # the X,Y size of the puzzle
        self.y_size = y_size

        self.width = 0                  # width of grid, set in the "paint" event
        self.height = 0                 # height of grid, set in the "paint" event
        self.grid_xspacing = 0          # X size of one cell, set in the "paint" event
        self.grid_yspacing = 0          # Y size of one cell, set in the "paint" event

        self.next_group = 1             # number of next group
        self.recycled_groups = []       # recycled groups

        self.selected_cells = set()     # set of selected cells: {(cx,cy), ...}
        self.editing_group = None       # number of group if we are drag editing
        self.group2cells = {}           # dictionary of group numnber -> list of cells:
                                        #   {0: [(cx,cy), (cx',cy')], ...}
                                        #    ^    ^^^^^
                                        #    |      |
                                        #    |      +-- coords of cell in group
                                        #    +-- group number
        self.cell2group = {}            # dictionary mapping cell coords to group number
                                        #   {(cx,cy): <groupnum>, ...}
        self.cell2value = {}            # dictionary mapping cell to value
        self.cell2guesses = {}          # dictionary mapping cell to a "guesses" list
        self.immutable_cells = set()    # set of (Cx,Cy) cells that are immutable

        self.shift_down = False         # True if SHIFT is held down

        self.focus_cell = None          # cell coords if focus set

        self.current_file = None        # path to current project file
        self.project_name = "<empty>"   # basename form of project file path

        self.dirty = False              # True if unsaved data, ie, "dirty"

#####
# utility routines
#####

    def error(self, msg):
        """MessageBox to show a critical error."""

        mbox = QMessageBox(self)
        mbox.setIcon(QMessageBox.Critical)
        mbox.setText(msg)
        mbox.setWindowTitle("Error")
        mbox.setStandardButtons(QMessageBox.Ok)
        mbox.exec_()

    def dump_formatted(self, state):
        """Format state object nicely."""

        return pformat(state, width=60, compact=True)

    def editor_log(self, label, state):
        """Log the state of the undo/redo system and current state."""

        log(f"{label} current state is:")
        log(self.dump_formatted(state))
        log("~" * 70)
        log("undo/redo system state:")
        log(self.editor.dump())
        log("~" * 70)

    def pack_state(self):
        """Pack the state into a sequence and return it."""

        state = (self.x_size, self.y_size, self.width, self.height,
                 self.grid_xspacing, self.grid_yspacing,
                 self.next_group, self.recycled_groups,
                 self.selected_cells, self.editing_group,
                 self.group2cells, self.cell2group, self.cell2value,
                 self.current_file, self.project_name, self.dirty,
                 self.immutable_cells)
        return copy.deepcopy(state)

    def unpack_state(self, state):
        """Unpack the state into the state variables."""

        (self.x_size, self.y_size, self.width, self.height,
         self.grid_xspacing, self.grid_yspacing,
         self.next_group, self.recycled_groups,
         self.selected_cells, self.editing_group,
         self.group2cells, self.cell2group, self.cell2value,
         self.current_file, self.project_name, self.dirty,
         self.immutable_cells) = state

    def debug(self):
        """Dump some debug information."""

        print("-------------------------------")
        print(f"{self.selected_cells=}")
        print(f"{self.group2cells=}")
        print(f"{self.cell2group=}")
        print(f"{self.cell2value=}")
        print(f"{self.cell2guesses=}")
        print(f"{self.immutable_cells=}")
        print("-------------------------------")
        print(f"{self.editor.dump()}")
        print("-------------------------------")

    def s2g_coords(self, sx, sy):
        """Convert screen coords to grid cell coords."""

        w_size = self.width // self.x_size
        h_size = self.height // self.y_size
        gx = math.ceil(sx // w_size)
        gy = math.ceil(sy // h_size)

        return (gx, gy)

    def g2s_coords(self, gx, gy):
        """Convert grid cell coords to screen coords."""

        w_size = self.width // self.x_size
        h_size = self.height // self.y_size
        sx = gx * w_size
        sy = gy * h_size

        return (sx, sy)

    def near_values(self, cell):
        """Return a set of values in cells around "cell", all directions."""

        result = set()

        (cx, cy) = cell
        for x in range(-1, 2):
            for y in range(-1, 2):
                other_cell = (cx+x, cy+y)
                if other_cell != cell:
                    # does neighbour cell have a value?
                    if other_cell in self.cell2value:
                        result.add(self.cell2value[other_cell])

        return result

#####
# undo/redo stuff
#####

    def editor_undo(self):
        """Save current state to redo_list and return popped undo_list."""

        if self.editor.can_undo():
            state = self.pack_state()
            new_state = self.editor.undo(state)
            self.unpack_state(new_state)

            self.editor_log("editor_undo: after: ", new_state)

    def editor_redo(self):
        """Redo and unpack next redo state."""

        if self.editor.can_redo():
            state = self.pack_state()
            new_state = self.editor.redo(state)
            self.unpack_state(new_state)

            self.editor_log("editor_redo: after: ", new_state)

    def calc_font_size(self):
        """Calculate a proper font size for the cell numbers'"""

        max_number = self.x_size * self.y_size
        text = f"{max_number}"
        self.font_size = 5
        min_spacing = min(self.grid_xspacing, self.grid_yspacing)

        while True:
            font = QFont('Terminus', self.font_size, QFont.Bold)
            fm = QFontMetrics(font)
            width = fm.width(text)
            if width < min_spacing*4//5:
                self.font_size += 1
            else:
                self.font_size -= 1
                break
        self.font = QFont('Terminus', self.font_size, QFont.Bold)
        self.gfont_size = self.font_size // 2
        self.gfont = QFont('Terminus', self.gfont_size, QFont.Bold)

    def resizeEvent(self, event=None):
        """Window resize, recalculate the number size."""

        size = self.size()
        self.width = size.width()
        self.height = size.height()
        self.grid_xspacing = self.width // self.x_size
        self.grid_yspacing = self.height // self.y_size

        self.calc_font_size()

    def open_file(self, filename):
        """Read the puzzle in *.sug form.

        filename  the path to the file to read

        We have to read all data into local variables first.
        If there's no error copy locals to widget variables.
        """
   
        # make a short filename string for reporting purposes
        file_base = os.path.basename(filename)

        # save the prior state, use it on a good "open"
        prior_state = self.pack_state()

        log(f"open_file: opening '{file_base}'")
        state = self.pack_state()
        self.editor_log("open_file: before: ", state)

        # read the file
        with open(filename) as fd:
            data = fd.readlines()

        if len(data) < 2:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        # get X and Y size from first line
        sizes = data[0].split()
        data = data[1:]
        if len(sizes) != 2:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        try:
            sizes = [int(x) for x in sizes]
        except ValueError:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        (x_size, y_size) = sizes    # the size of the new puzzle
        group2cells = {}            # set locals to "empty" values
        cell2group = {}
        cell2value = {}
        selected_cells = set()
        immutable_cells = set()

        if x_size < 1 or y_size < 1:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        if x_size > self.maxsize or y_size > self.maxsize:
            self.error(f"File {file_base} data is too big.")
            return

        if len(data) != y_size:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        max_group = 0
        for (row, line) in enumerate(data):
            line = line.split()
            if len(line) != x_size:
                self.error(f"File {file_base} isn't a valid Suguru file.")
                return
            for (col, item) in enumerate(line):
                fields = item.split(",")
                if len(fields) != 2:
                    self.error(f"File {file_base} isn't a valid Suguru file.")
                    return
                try:
                    fields = [int(item) for item in fields]
                except ValueError:
                    self.error(f"File {file_base} isn't a valid Suguru file.")
                    return

                (grpnum, value) = fields    # remember largest group number
                if grpnum > max_group:
                    max_group = grpnum

                cell = (col, row)
                if grpnum > 0:
                    if grpnum not in group2cells:
                        group2cells[grpnum] = []
                    group2cells[grpnum].append(cell)
                    cell2group[cell] = grpnum
                    selected_cells.add(cell)
                if value > 0:
                    cell2value[cell] = value
                    immutable_cells.add(cell)

        ######
        # Now update the widget variables
        ######

        self.x_size = x_size
        self.y_size = y_size
        self.group2cells = group2cells
        self.cell2group = cell2group
        self.cell2value = cell2value
        self.selected_cells = selected_cells
        self.immutable_cells = immutable_cells

        # set the "next group" mechanism up
        # this leaves "holes" in the groups that can be allocated
        # could try to identify group nunmbres < max_group that aren't used?
        self.next_group = max_group + 1
        self.recycled_groups = []

        # set the project filepath
        self.current_file = filename
        (self.project_name, _) = os.path.splitext(os.path.basename(filename))

        # reset the "dirty" flag
        self.dirty = False

        # set the appropriate font size
        self.resizeEvent()

        # "open" successful, save prior state to undo/redo
        self.editor.save(prior_state)
        self.editor_log("open_file: after: ", self.pack_state())

    def save_file(self, filename):
        """Save the puzzle in *.sug form.

        filename  the path to the file to write
        """

        # make a short filename string for reporting purposes
        file_base = os.path.basename(filename)

        log(f"Saving to file: '{file_base}'")

        # get maximum group number, figure max group string width
        max_grp = 1
        if self.group2cells:
            max_grp = max(self.group2cells.keys())
        max_glen = len(f"{max_grp}")

        # get maximum value, figure max value string width
        max_val = 1
        if self.cell2value:
            max_val = max(self.cell2value.values())
        max_vlen = len(f"{max_val}")

        # write data to the file
        with open(filename, "w") as fd:
            fd.write(f"{self.x_size} {self.y_size}\n")
            for row in range(self.y_size):
                for col in range(self.x_size):
                    cell = (col, row)
                    group = self.cell2group.get(cell, 0)
                    cell_value = self.get_cell_value(cell)
                    fd.write(f"{group:{max_glen}d},{cell_value:<{max_vlen}d} ")

                fd.write("\n")

        # set the project filepath
        self.current_file = filename
        (self.project_name, _) = os.path.splitext(os.path.basename(filename))

        # reset the "dirty" flag
        self.dirty = False

#####
# drawing methods
#####

    def draw_cell_outline(self, qp, x, y, side):
        """Draw a group "edge" line for a grid cell.

        qp    the canvas to draw on
        x, y  the cell coordinates
        side  one of "t", "b", "l" or "r" (the side)
        """

        # get top-left coordinate of cell from cell x,y
        tl_x = self.grid_xspacing * x
        tl_y = self.grid_yspacing * y

        # draw thicker side outline
        if side == "t":
            start_x = tl_x
            start_y = tl_y
            stop_x = start_x + self.grid_xspacing
            stop_y = start_y
        elif side == "r":
            start_x = tl_x + self.grid_xspacing
            start_y = tl_y
            stop_x = start_x
            stop_y = start_y + self.grid_yspacing
        elif side == "b":
            start_x = tl_x
            start_y = tl_y + self.grid_yspacing
            stop_x = start_x + self.grid_xspacing
            stop_y = start_y
        elif side == "l":
            start_x = tl_x
            start_y = tl_y
            stop_x = start_x
            stop_y = start_y + self.grid_yspacing
        else:
            raise ValueError(f"Got illegal 'side' value: {side}")

        qp.drawLine(start_x, start_y, stop_x, stop_y)

#####
# cell editing functions
#####

    def edit_value(self, value, cell):
        """Set the value of a cell. 0 means remove value."""

        # set value of cell to "value", 0 means remove value
        if value == 0:
            if cell in self.cell2value:
                del self.cell2value[cell]
                return True
        else:
            self.cell2value[cell] = value
            return True
        return False

    def get_cell_value(self, cell):
        """Get the value in "cell"."""

        return self.cell2value.get(cell, 0)

    def toggle_guesses(self, value, cell):
        """Toggle a value to the guesses for a cell.

        If 'value' is a guess remove it.  If it isn't a
        value add it.
        """

        guesses = self.cell2guesses.get(cell, set())
        if value in guesses:
            guesses.remove(value)
        else:
            guesses.add(value)
        self.cell2guesses[cell] = guesses

    def clear_guesses(self, cell):
        """Clear all guesses from a cell."""

        if cell in self.cell2guesses:
            del self.cell2guesses[cell]

#####
# over-ridden widget methods
#####

    def mousePressEvent(self, event):
        """Catch mouse button DOWN event."""

        # get current mouse position
        mx = event.x()
        my = event.y()

        # the button that was pressed
        button = event.button()

        # figure out what cell was clicked from click screen coords
        cell = self.s2g_coords(mx, my)

        # save prior state, don't push to undo/redo yet
        prior_state = self.pack_state()

        # if left DOWN focus on cell
        if button == Qt.LeftButton:
            # just set the focus
            self.focus_cell = cell
        elif button == Qt.MidButton:
#            print(f"mousePressEvent: Middle: {mx=}, {my=}, {event=}")
            self.focus_cell = cell
        elif button == Qt.RightButton:
            # value/guess edit
            self.focus_cell = cell

            grpnum = self.cell2group[cell]
            cell_list = self.group2cells[grpnum]
            num_cells = len(cell_list)
            used_values = set(self.cell2value.get(c, 0) for c in cell_list)
            cell_guesses = self.cell2guesses.get(cell, [])
            near_values = self.near_values(self.focus_cell)
            all_used = used_values | near_values
            allowed_values = {val for val in range(1, num_cells+1) if val not in all_used}

            # right click on a cell, edit if not immutable
            if cell not in self.immutable_cells:
                if self.shift_down:     # is SHIFT down?
                    # yes, show checkbox dialof
                    guess_list = []
                    for anum in range(1, num_cells+1):
                        if anum in allowed_values:
                            guess_list.append((anum, anum in cell_guesses))
    
                    dlg = EditGuess(self, guess_list)
                    if not dlg.exec():
                        return
                    guesses = dlg.get_state()
                    self.cell2guesses[cell] = [val for (val, state) in guesses if state]
                else:                   # SHIFT not down
                    # show "value" context menu
                    context_menu = QMenu(self)
                    a = context_menu.addAction("remove")
                    if not self.cell2value.get(cell, 0):
                        a.setEnabled(False)
                    a.triggered.connect(lambda chk, x=0, c=cell: self.edit_value(x, c))
    
                    for anum in range(1, num_cells+1):
                        a = context_menu.addAction(f"insert {anum}")
                        a.triggered.connect(lambda chk, x=anum, c=cell: self.edit_value(x, c))
                        if anum in used_values or anum in self.near_values(cell):
                            a.setEnabled(False)
        
                    # display context menu around the click point
                    result = context_menu.exec(self.mapToGlobal(event.pos()))
        
                    # if edited cell, save
                    if result:
                        self.editor.save(prior_state)
                        self.c.update_undoredo.emit()   # tell app code to update undo/redo menu

        # show changes to the view, if any
        self.repaint()

    def mouseReleaseEvent(self, event):
        """Catch mouse button UP event."""

        # get current mouse position
#        mx = event.x()
#        my = event.y()

        # the button that was pressed
        button = event.button()

        # if left button up then ...
        if button == Qt.LeftButton:
            pass
        elif button == Qt.RightButton:
            pass
#            print(f"mouseReleaseEvent: Right: {mx=}, {my=}, {event=}")
        elif button == Qt.MidButton:
            pass
#            print(f"mouseReleaseEvent: Middle: {mx=}, {my=}, {event=}")

    def keyPressEvent(self, event):
        """Capture a key press."""

        if not self.focus_cell:
            return                  # ignore all keystrokes if we don't have a focus

        # in case we set a value/guess, find what we currently have
        grpnum = self.cell2group[self.focus_cell]
        cell_list = self.group2cells[grpnum]
        num_cells = len(cell_list)
        used_values = set(self.cell2value.get(c, 0) for c in cell_list)
        near_values = self.near_values(self.focus_cell)
        all_used = used_values | near_values
        allowed_values = {val for val in range(1, num_cells+1) if val not in all_used}

        # now see what key press we have
        key = event.key()
        scancode = event.nativeScanCode()

        if key == Qt.Key_Shift:
            self.shift_down = True
        elif key == Qt.Key_Delete or key == Qt.Key_Backspace:
            if self.shift_down:
                if self.focus_cell not in self.cell2value:
                    self.clear_guesses(self.focus_cell)
            else:
                if (self.focus_cell in self.cell2value
                        and self.focus_cell not in self.immutable_cells):
                    self.edit_value(0, self.focus_cell)
        elif 10 <= scancode <= 18:
            # a numeric key, 1..9
            value = scancode - 9
            if value <= num_cells:
                if self.shift_down:
                    if self.focus_cell not in self.cell2value:
                        if value in allowed_values:
                            self.toggle_guesses(value, self.focus_cell)
                else:
                    if self.focus_cell not in self.immutable_cells:
                        if value in allowed_values:
                            self.clear_guesses(self.focus_cell)
                            self.edit_value(value, self.focus_cell)
        elif key in self.key_deltas:
            # arrow key, move the focus
            (dx, dy) = self.key_deltas[key]
            (cx, cy) = self.focus_cell

            cx += dx + self.x_size
            cy += dy + self.y_size
            self.focus_cell = (cx % self.x_size, cy % self.y_size)

        self.repaint()

    def keyReleaseEvent(self, event):
        """Capture a key release."""

        key = event.key()
        if key == Qt.Key_Shift:
            self.shift_down = False

    def paintEvent(self, e):
        """Handle the widget 'paint' event.

        Check widget size, then draw objects on the canvas.
        """

        qp = QPainter()
        qp.begin(self)

        size = self.size()
        self.width = size.width()
        self.height = size.height()
        self.grid_xspacing = self.width // self.x_size
        self.grid_yspacing = self.height // self.y_size

        qp.setRenderHint(QPainter.Antialiasing)

        actual_width = self.x_size*self.grid_xspacing
        actual_height = self.y_size*self.grid_yspacing

        # draw grid
        pen = QPen(Suguru.GridColour, 0.5, Qt.DotLine)
        qp.setPen(pen)
        qp.setBrush(Suguru.CanvasBG)
        for x in range(self.grid_xspacing, self.width, self.grid_xspacing):
            qp.drawLine(x, 0, x, actual_height)
        for y in range(self.grid_yspacing, self.height, self.grid_yspacing):
            qp.drawLine(0, y, actual_width, y)

        # draw the group background colour
        pen = QPen(Suguru.GridColour, 0, Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(Suguru.GroupBG)
        for ((gx, gy), _) in self.cell2group.items():
            (sx, sy) = self.g2s_coords(gx, gy)
            qp.drawRect(sx+1, sy+1,     # leave a little outline of background
                        self.grid_xspacing-1, self.grid_yspacing-1)

        # if focus is set, draw a cell of different background color
        if self.focus_cell:
            qp.setBrush(Suguru.FocusBG)
            (gx, gy) = self.focus_cell
            (sx, sy) = self.g2s_coords(gx, gy)
            qp.drawRect(sx+1, sy+1,
                        self.grid_xspacing-1, self.grid_yspacing-1)

        # draw canvas outline
        pen = QPen(Suguru.WidgetOutlineColour, 2, Qt.SolidLine)
        qp.setPen(pen)
        actual_width = self.x_size*self.grid_xspacing
        actual_height = self.y_size*self.grid_yspacing
        qp.drawLine(0, 0, actual_width, 0)                          # top
        qp.drawLine(0, actual_height, actual_width, actual_height)  # bottom
        qp.drawLine(actual_width, actual_height, actual_width, 0)   # right
        qp.drawLine(0, 0, 0, actual_height)                         # left

        # draw outlines around groups, draw group BG colour
        pen = QPen(Suguru.GroupOutlineColour, 1, Qt.SolidLine)
        qp.setPen(pen)
        for ((x, y), grpnum) in self.cell2group.items():
            # check all neighbours of this cell
            if self.cell2group.get((x, y-1), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "t")
            if self.cell2group.get((x, y+1), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "b")
            if self.cell2group.get((x-1, y), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "l")
            if self.cell2group.get((x+1, y), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "r")

        # put values into cells that have them
        # draw immutable valies in different colour
        qp.setFont(self.font)
        for (cell, value) in self.cell2value.items():
            if cell in self.immutable_cells:
                pen = QPen(Suguru.ImmutableColour, 0)
            else:
                pen = QPen(Suguru.TextColour, 0)
            qp.setPen(pen)
            (x, y) = cell                       # cell coords
            rect = QRect(x*self.grid_xspacing, y*self.grid_yspacing, self.grid_xspacing, self.grid_yspacing)
            qp.drawText(rect, Qt.AlignHCenter|Qt.AlignVCenter, f"{value}")

        # put guesses into cells that have them
        pen = QPen(Suguru.GuessColour, 0)
        qp.setPen(pen)
        qp.setFont(self.gfont)
        for (cell, guesses) in self.cell2guesses.items():
            (x, y) = cell                       # cell coords
            for value in guesses:
                xoffset = x * self.grid_xspacing + self.grid_xspacing // 3 * ((value-1) % 3)
                yoffset = y * self.grid_yspacing + self.grid_yspacing // 3 * ((value-1) // 3)
                rect = QRect(xoffset, yoffset, self.grid_xspacing//3, self.grid_yspacing//3)
                qp.drawText(rect, Qt.AlignHCenter|Qt.AlignVCenter, f"{value}")

        qp.end()

