#!/usr/bin/env python

"""
A Suguru puzzle player using the "suguru.py" widget.
"""

import sys
import math
import os.path
from PyQt5.QtCore import QCoreApplication, QTimer
import PyQt5.QtCore as QtCore
from PyQt5.QtGui import QPalette, QColor, QIcon
from PyQt5.QtWidgets import (QMainWindow, QWidget, QApplication, QVBoxLayout,
                             QGridLayout, QLabel, QLineEdit, QVBoxLayout,
                             QAction, QFileDialog, QMessageBox, QDialog,
                             QDialogButtonBox, QFormLayout, QSpinBox)

from suguru import Suguru
import logger
log = logger.Log('player.log', logger.Log.DEBUG)


###############################################################################
# The Test application
###############################################################################

class Test(QMainWindow):
    def __init__(self):
        super().__init__()

        # set the size of the demo window, etc
        self.setGeometry(100, 100, 800, 830)
        self.setWindowIcon(QIcon("suguru_icon.png"))
        self.setMinimumWidth(300)
        self.setMinimumHeight(330)

        # start drawing the GUI
        layout = QVBoxLayout()

        self.suguru = Suguru(self, 10, 10)
        layout.addWidget(self.suguru)
        self.suguru.setFocusPolicy(QtCore.Qt.ClickFocus) # so widget gets KeyEvents

        # set up the menu
        self.init_menu()

        # connect update_undoredo signal to handler
        self.suguru.c.update_undoredo.connect(self.update_undoredo_menu)

        # don't know why this is required???
        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        self.setWindowTitle(f"Suguru {self.suguru.version}")

    def init_menu(self):
        menu_bar = self.menuBar()

        file_menu = menu_bar.addMenu('&File')
        edit_menu = menu_bar.addMenu('&Edit')
        help_menu = menu_bar.addMenu('&Help')

        # open menu item
        open_action = QAction('&Open...', self)
        open_action.triggered.connect(self.file_open)
        open_action.setStatusTip('Open a puzzle')
        open_action.setShortcut('Ctrl+O')
        file_menu.addAction(open_action)

        # save menu item
        save_action = QAction('&Save', self)
        save_action.setStatusTip('Save the puzzle')
        save_action.setShortcut('Ctrl+S')
        save_action.triggered.connect(self.file_save)
        file_menu.addAction(save_action)

        # save as menu item
        saveas_action = QAction('Save &As', self)
        saveas_action.setStatusTip('Save As the puzzle')
        saveas_action.setShortcut('Ctrl+A')
        saveas_action.triggered.connect(self.file_saveas)
        file_menu.addAction(saveas_action)

        file_menu.addSeparator()

        # exit menu item
        exit_action = QAction('&Exit', self)
        exit_action.setStatusTip('Exit')
        exit_action.setShortcut('Alt+F4')
        exit_action.triggered.connect(self.quit)
        file_menu.addAction(exit_action)

        #####
        # edit menu
        self.undo_action = QAction('&Undo', self)
        self.undo_action.setStatusTip('Undo')
        self.undo_action.setShortcut('Ctrl+Z')
        self.undo_action.triggered.connect(self.edit_undo)
        edit_menu.addAction(self.undo_action)
        self.undo_action.setEnabled(False)

        self.redo_action = QAction('&Redo', self)
        self.redo_action.setStatusTip('Redo')
        self.redo_action.setShortcut('Ctrl+Y')
        self.redo_action.triggered.connect(self.edit_redo)
        edit_menu.addAction(self.redo_action)
        self.redo_action.setEnabled(False)

        edit_menu.addSeparator()

        # SCREENSHOT menuitem
        debug_action = QAction('Screenshot', self)
        debug_action.setStatusTip('Screenshot')
        debug_action.triggered.connect(self.take_screenshot)
        edit_menu.addAction(debug_action)

        # DEBUG menuitem
        debug_action = QAction('Debug', self)
        debug_action.setStatusTip('Debug')
        debug_action.triggered.connect(self.debug)
        edit_menu.addAction(debug_action)

        #####
        # file menu
        help_action = QAction('Help', self)
        help_action.setStatusTip('Help')
        help_action.setShortcut('F1')
        help_action.triggered.connect(self.help_help)
        help_menu.addAction(help_action)

    def help_help(self):
        print("HELP")

    def debug(self):
        self.suguru.debug()

    def yn_choice(self, msg):
        """A simple Yes/No warning message box."""

        mbox = QMessageBox(self)
        mbox.setIcon(QMessageBox.Question)
        mbox.setText(msg)
        mbox.setWindowTitle("Warning")
        mbox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        button = mbox.exec_()
        return button

    def choice_cancel(self, msg):
        """A simple Cancel/Yes/No warning message box."""

        mbox = QMessageBox(self)
        mbox.setIcon(QMessageBox.Question)
        mbox.setText(msg)
        mbox.setWindowTitle("Warning")
        mbox.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        button = mbox.exec_()
        return button

    def closeEvent(self, event):
        """Override close event, save if "dirty".

        self.quit() does all the work.
        """

        self.quit()
        event.ignore()  # if we return, ignore the close

    def quit(self):
        """Close the app, save if "dirty"."""

        if self.suguru.dirty:
            button = self.choice_cancel("There is unsaved data.  Do you want to save it?")

            if button == QMessageBox.Cancel:
                return
            if button == QMessageBox.Yes:
                if self.suguru.current_file:
                    self.suguru.save_file(self.suguru.current_file)
                else:
                    self.file_saveas()

        QCoreApplication.quit()

    def file_new(self):
        """Create a new project.

        If unsaved data offer to save it.
        """

        # get the required size of the new puzzle
        dlg = PuzzleSize(self, x=10, y=10, maxsize=Suguru.MaxSize)
        if not dlg.exec():
            return
        (x_size, y_size) = dlg.get_xy()

        # save the current project if dirty
        if self.suguru.dirty:
            button = self.yn_choice("The current project is unsaved.  Save it?")
            if button == QMessageBox.Yes:
                if self.suguru.current_file:
                    self.suguru.save_file(self.suguru.current_file)
                else:
                    self.file_saveas()

        # create the new data
        self.suguru.new_file(x_size, y_size)
        self.setWindowTitle(f"Suguru Editor {self.suguru.version} "
                            f"({self.suguru.x_size}x{self.suguru.y_size}) - "
                            f"{self.suguru.project_name}")

        self.update_undoredo_menu()

    def file_open(self):
        """Load a new file into the player.

        If the current project is unsaved offer to save it.
        """

        # offer to saved unsaved project
        if self.suguru.dirty:
            button = self.yn_choice("The current project is unsaved.  Save it?")
            if button == QMessageBox.Yes:
                if self.suguru.current_file:
                    self.suguru.save_file(self.suguru.current_file)
                else:
                    self.file_saveas()

        # get the file to open
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        (filename, _) = QFileDialog.getOpenFileName(self, "Open Suguru Project",
                                                    "", "Suguru Files (*.sug);;All Files (*)",
                                                    options=options)
        if filename:
            self.suguru.open_file(filename)
            self.setWindowTitle(f"Suguru Editor {self.suguru.version} "
                                f"({self.suguru.x_size}x{self.suguru.y_size}) - "
                                f"{self.suguru.project_name}")

        self.update_undoredo_menu()

    def file_save(self):
        """Save the current project.

        If no assumed filename, ask for one.
        """

        if self.suguru.current_file:
            self.suguru.save_file(self.suguru.current_file)
        else:
            self.file_saveas()

        self.update_undoredo_menu()

    def file_saveas(self):
        """Save the current project.

        No project filename, so ask for one.
        """

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        (filename, _) = QFileDialog.getSaveFileName(self, "Save Suguru File As", "",
                                                    "Suguru Files (*.sug)"
                                                        ";;All Files (*)",
                                                    options=options)
        if filename:
            if filename[-4:] != ".sug":
                filename += ".sug"              # add .sug type if necessary
            self.suguru.save_file(filename)
            self.setWindowTitle(f"Suguru Editor {self.suguru.version} "
                                f"({self.suguru.x_size}x{self.suguru.y_size}) - "
                                f"{self.suguru.project_name}")

        self.update_undoredo_menu()

    def update_undoredo_menu(self):
        """Set the undo/redo Edit menuitems as active or disabled."""

        active = self.suguru.editor.can_undo()
        self.undo_action.setEnabled(active)

        active = self.suguru.editor.can_redo()
        self.redo_action.setEnabled(active)

    def edit_undo(self):
        """Perform an UNDO."""

        self.suguru.editor_undo()
        self.setWindowTitle(f"Suguru Editor {self.suguru.version} "
                            f"({self.suguru.x_size}x{self.suguru.y_size}) - "
                            f"{self.suguru.project_name}")
        self.update_undoredo_menu()
        self.suguru.repaint()

    def edit_redo(self):
        """Perform a REDO."""

        self.suguru.editor_redo()
        self.setWindowTitle(f"Suguru Editor {self.suguru.version} "
                            f"({self.suguru.x_size}x{self.suguru.y_size}) - "
                            f"{self.suguru.project_name}")
        self.update_undoredo_menu()
        self.suguru.repaint()

    def take_screenshot(self):
        """Run on menuitem choice, wait a bit then take snapshot."""

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        (filename, _) = QFileDialog.getSaveFileName(self, "Save Snapshot As", "",
                                                    "PNG Files (*.png)"
                                                        ";;All Files (*)",
                                                    options=options)
        if filename:
            if filename[-4:] != ".png":
                filename += ".png"              # add .sug type if necessary

        QTimer.singleShot(150, lambda : self.take_screenshot_after(filename))

    def take_screenshot_after(self, filename):
        """Take a snapshot of the application window."""

        screen = QApplication.primaryScreen()
        screenshot = screen.grabWindow(self.winId() )
        screenshot.save(filename)

    def help_about(self):
        """Show some user help, possibly in the browser."""

        print(f"help_about:")   # just for now


# start the app
app = QApplication(sys.argv)
ex = Test()
ex.show()
sys.exit(app.exec_())
