# Display a nicely formatted puzzle

In the previous page we read in the puzzle description data and
got a list of lists:

    (1,0) (1,0) (2,1) (2,0) (2,0) (3,0)
    (1,2) (1,0) (1,0) (2,0) (2,0) (3,5)
    (4,0) (4,0) (4,0) (5,0) (5,0) (6,0)
    (4,1) (4,0) (6,0) (5,0) (3,0) (3,0)
    (7,0) (6,0) (6,0) (6,0) (8,0) (8,0)
    (7,0) (7,0) (6,1) (8,0) (8,2) (8,3)

where each cell is a tuple (group, value) where the *group* is the unique group
number and the *value* is the number in the cell.  A cell value of 0 means there
is nothing in the cell.

## Nicely displaying the puzzle

We certainly want to print the puzzle once we have solved it and it's a nice
feature if we can print the puzzle in a pleasing way.  It's also useful as
a debug aid.  For an example of what we want, the data in the file *puzzle_1.sug*
could be printed like this:

    +-------+-----------+---+
    | .   . | 1   .   . | . |
    |       +---+       |   |
    | 2   .   . | .   . | 5 |
    + ----------+-------+   |
    | .   .   . | .   . | . |
    |       +---+   +---+   |
    | 1   . | . | . | .   . |
    +---+---+   +---+-------+
    | . | .   .   . | .   . |
    |   +---+   +---+       |
    | .   . | 1 | .   2   3 |
    +-------+---+-----------+

Later we might want to display this more nicely in a GUI, but this will
do for now.

So, how do we produce that nice display from the list of lists?  This code
will be complex, and it's very important that you approach this in easy steps.
Just rushing in **WILL** end up as a dog's breakfast.

The first thing to note is that each cell consists of 3 characters in the X
direction.  The first and last characters are blank and the middle character
is either a "." if the cell is empty (value 0) or is a non-zero number
1 ... 5.  So to write one row, say the top row above, we would start with
a left edge character of "|" and then 3 cell value characters for each cell.
Between each cell we have either a space or a "|" character.  We delimit
with the "|" character whenever the group number of two cells differ.

There are other display rows between the cell rows, but we can make a start
and ignore those rows at first.  Similarly, we don't worry about the edges,
top, bottom, left or right.

Writing a function to show the puzzle, we have this as a start:

    def show_puzzle(puzzle, x_size, y_size):
        """Display the puzzle in a nice way.
    
        puzzle  the list of lists describing the puzzle
        x_size  X size of the puzzle
        y_size  Y size of the puzzle
        """
    
        for row in puzzle:
            # print cell contents and right-side divider of each cell
            for (i, cell) in enumerate(row):
                (group, value) = cell
                print(f" {value if value > 0 else '.'} ", end="")
                if i <= (x_size-2):    # if there is a cell to the right
                    (right_group, _) = row[i+1]
                    print("|" if (right_group != group) else " ", end="")
            print()

when we call the function to show the puzzle after we loaded it from the
file we get:

     .   . | 1   .   . | . 
     2   .   . | .   . | 5 
     .   .   . | .   . | . 
     1   . | . | . | .   . 
     . | .   .   . | .   . 
     .   . | 1 | .   2   3 

So far, so good.  Note the use of the *end=""* option for *print()*.
This allows us to build a line piece-meal and not have a newline printed
when we don't want it.  This technique is heavily used in this function.

The next step is to write separator lines between each cell
row.  This is a bit more complicated than just printing each cell row
because we have to consider the neighbours in the *following* row, not just the
next cell in a row.

Again, we will simplify a little and just consider horizontal dividers between
each cell, so we only have to draw either a "---" divider or a "   " divider. 
We will ignore the "+" characters at the corners of cells for now.  

This code prints rows and each "inbetween" divider row.  We are still ignoring
the edges:

    for (i, row) in enumerate(puzzle):
        # print cell contents and right-side divider of each cell
        for (j, cell) in enumerate(row):
            (group, value) = cell
            print(f" {value if value > 0 else '.'} ", end="")
            if j <= (x_size - 2):   # if there is a cell to the right
                (right_group, _) = row[j+1]
                print("|" if (right_group != group) else " ", end="")
        print()

        # now print the lower divider row if there is a cell row below
        for (j, cell) in enumerate(row):
            (group, value) = cell
            if i <= (y_size - 2):   # if there is a puzzle row below
                (below_group, _) = puzzle[i+1][j]
                print(f"{'--- ' if (below_group != group) else '    '}", end="")
        print()

This prints:

     .   . | 1   .   . | . 
            ---             
     2   .   . | .   . | 5 
    --- --- --- --- ---     
     .   .   . | .   . | . 
            ---     ---     
     1   . | . | . | .   . 
    --- ---     --- --- --- 
     . | .   .   . | .   . 
        ---     ---         
     .   . | 1 | .   2   3 

which is fine.

The next step is to add the "|" or "+" characters at the edges and corners
of cells in the current row and the corresponding cells in the next row.

When printing the in-between rows we only have to consider four cells: the
current cell in a row, that cell's right neighbour, and the two
cells in the next row immediately below those two cells.  There are a few
cases, and the drawings below show the *group number* in each of the cases, 
not the cell *values*.  The comment says what 4 delimiter characters are
printed under the top-left cell:

     1   1 
            	3 spaces and a space corner character
     1   1 
     

     1 | 2 
       |   	3 spaces and a "|" corner character
     1 | 2 


     1   1 
    -------	3 "-" and a "-" corner character
     2   2 


     1 | 2 
       +---	3 spaces and a "+" corner character
     1   1 



     1 | 1 
       +---	3 spaces and a "+" corner character
     1 | 2 


     1 | 1 
    ---+   	3 "-" and a "+" corner character
     2 | 1 


     1 | 2 
    ---+   	3 "-" and a "+" corner character
     2   2 

So when printing that single cell corner character we only have to consider the
three neighbours of a cell and print the appropriate "corner" character
according to this pseudocode:

    if cell == below and cell == right and cell == below right:
        corner = " "
    elif cell == below and right == below right and cell != right:
        corner =  "|"
    elif cell == right and below == below right and cell != below:
        corner = "-"
    else:
        corner = "+"
    
Putting that into the code that prints the "between" lines, we have:

        for (j, cell) in enumerate(row):
            (group, value) = cell
            if i <= (y_size - 2):   # if there is a puzzle row below
                (below_group, _) = puzzle[i+1][j]
                print(f"{'---' if (below_group != group) else '   '}", end="")
                if j <= (x_size - 2):
                    (right_group, _) = puzzle[i][j+1]
                    (below_right_group, _) = puzzle[i+1][j+1]
                    if group == below_group and group == right_group and group == below_right_group:
                        corner = " "
                    elif group == below_group and right_group == below_right_group and group != right_group:
                        corner = "|"
                    elif group == right_group and below_group == below_right_group and group != below_group:
                        corner = "-"
                    else:
                        corner = "+"
                else:
                    corner = " "
                print(corner, end="")
        print()

which produces this:

     .   . | 1   .   . | . 
           +---+       |    
     2   .   . | .   . | 5 
    -----------+-------+    
     .   .   . | .   . | . 
           +---+   +---+    
     1   . | . | . | .   . 
    ---+---+   +---+------- 
     . | .   .   . | .   . 
       +---+   +---+        
     .   . | 1 | .   2   3 

Good!  Now we need to add the left and right edges to the cell rows and the
"between" rows.  The edge characters will always be "|" for every cell line.
The "between" line will have "|" except when the leftmost cell group isn't
the same as the cell group of the following row.  In that case it will be "+".

Similarly, the top and bottom edge lines will always have "---" on cell
columns, but the edge character will be a "-" if the cell and the one to the
right are the same, else it will be "+".

The four corner characters are always "+".

Putting all that together the `show_display()` function becomes:

   def show_puzzle(puzzle, x_size, y_size):
       """Display the puzzle in a nice way.
   
       puzzle  the list of lists describing the puzzle
       x_size  X size of the puzzle
       y_size  Y size of the puzzle
       """
       print(f"{puzzle=}")
   
       # print the top edge line
       print("+", end="")  # the top-left corner
       for (i, cell) in enumerate(puzzle[0]):
           if i <= (x_size - 2):
               (group, _) = cell
               (right_group, _) = puzzle[0][i+1]
               print(f"---{'-' if group == right_group else '+'}", end="")
       print("---+")
   
       for (i, row) in enumerate(puzzle):
           print("|", end="") # start the cell lime
   
           # print cell contents and right-side divider of each cell
           for (j, cell) in enumerate(row):
               (group, value) = cell
               print(f" {value if value > 0 else '.'} ", end="")
               if j <= (x_size - 2):   # if there is a cell to the right
                   (right_group, _) = row[j+1]
                   print("|" if (right_group != group) else " ", end="")
   
           print("|")  # end the line
   
           # now print the following divider row if there is a cell row below
           if i <= (y_size - 2):   # if there is a puzzle row below
               (left_group, _) = row[0]
               (below_group, _) = puzzle[i+1][0]
               if left_group != below_group: # if first cell group and next first group not same
                   print("+", end="")
               else:
                   print("|", end="")
   
               for (j, cell) in enumerate(row):
                       (group, value) = cell 
                       (below_group, _) = puzzle[i+1][j]
                       print(f"{'---' if (below_group != group) else '   '}", end="")
                       if j <= (x_size - 2):
                           (right_group, _) = puzzle[i][j+1]
                           (below_right_group, _) = puzzle[i+1][j+1]
                           if (group == below_group and group == right_group
                                   and group == below_right_group):
                               corner = " "
                           elif (group == below_group and right_group == below_right_group
                                   and group != right_group):
                               corner = "|"
                           elif (group == right_group and below_group == below_right_group
                                   and group != below_group):
                               corner = "-"
                           else:
                               corner = "+"
                           print(corner, end="")
               (left_group, _) = row[-1]
               (below_group, _) = puzzle[i+1][-1]
               print("+" if left_group != below_group else "|")
   
       # print the bottom edge line
       print("+", end="")  # the top-left corner
       for (i, cell) in enumerate(puzzle[-1]):
           if i <= (x_size - 2):
               (group, _) = cell
               (right_group, _) = puzzle[-1][i+1]
               print(f"---{'-' if group == right_group else '+'}", end="")
       print("---+")

and that produces the output we showed at the very top.

That's quite a bit of code!  This is why I suggested that a Sudoku solver
should be tried first.  The code needed to read and display the board is
much easier for Sudoku.  The "solving" code is very similar for both puzzles.

I write the code to display the board first because it can be used as
a "debug" tool while working on the code to solve the puzzle.  Make sure
the debug tools work properly before using them, it makes life easier!


[The complete code for this section is here](Suguri.01.py).
In [the next part of the etude](Suguru.02.md), we start solving the puzzle.
