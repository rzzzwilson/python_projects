# Suguru file format

A file containing a Suguru puzzle will have an extension *.sug*.

A Suguru puzzle file is a text file.  The first line of the file
contains the dimensions of the puzzle.  For example, this first
line:

    10 15

indicates that the puzzle is 10 cells wide and 15 cells high.  Any
amount of whitespace may appear between the two numbers.

The following lines contain information describing each cell of
the puzzle.  Each line contains all the information for the
corresponding row of cells of the puzzle.  So a puzzle with
dimensions "10 15" would have 15 rows each of 10 cells.

Each of the cell descriptions in a row has the form:

    group,value

The `group` number is an integer that describes which group of
cells the cell belongs to.  All cells with the same group number are
one group of connected cells.  A group number of *0* means that 
the cell isn't yet part of a group.  A complete puzzle will not have
any cells with a group number *0*.  A partially completed puzzle will.

The `value` of a cell is an integer showing the number that is
pre-defined in the cell.  If the *value* number is *0* there is no
value in that cell.

This image shows an incomplete puzzle in the editor:

![incomplete 4x6_puzzle](puzzle_4x6.png "An incomplete 4x6 puzzle")

This is the *.sug* file written when the above puzzle is saved:

    4 6
    1,0 1,0 3,0 4,2 
    2,0 1,1 3,0 4,0 
    2,0 2,0 3,0 4,0 
    2,0 3,0 3,5 5,0 
    0,0 0,0 5,0 5,0 
    0,0 0,0 0,0 0,0 

Note that the amount of whitespace between fields is not important.
Puzzle files produced by the editor are formatted so as to be easily
read by eye.
