# Solving the puzzle

Now it's time to think about solving the puzzle.

We will use the "standard" approach for this kind of puzzle.  Look for
an unfilled cell, choose all possible numbers that can be put into that
cell, iterate through the possible numbers, placing each one and recursively
calling the solver to find the next empty cell.  If a recursive call returns
choose the next number and try again.  When all possible numbers fail, return
and let the previous call try another number.

# Find the next empty cell

We do this by scanning the grid of cells from the top-left, scanning to the
right.  Pretty easy.

# Generate candidate numbers

Once we have found an empty cell we need to know what numbers we can fill
that cell with.  To help us do this we create a dictionary of groups.  The Nth
group will will have a key of N in the dictionary.  Values for each key will
be a tuple containing the size of the group and a set containing the currently
displayed numbers.  So the `groups` dictionary might look like this:

    groups = {0: (4, {2, 3}), 1: (5, {2, 4}), 2: (3, {})}

That shows we have 3 groups of sizes 4, 5 and 3 respectively.  Group 1 is size
5 and currently is showing the numbers 2 and 4.  Group 2 is size 3 and currently
has no numbers displayed.

It's convenient if the function that reads a puzzle file and generates the
`puzzle` data structure also creates and returns the `groups` dictionary.

# Test candidate numbers

After choosing a possible number to fill a cell we next have to determine if
the possible number is legal, that is, are there any neighbour cells that also
contain that number?  If there are, the number is disqualified, move
to the next possible number.

This "neighbour" check is done using the `puzzle` board representation.  We
know the empty cell X and Y coordinates in the board so we can easily check
all possible neighbour cells.

# Recursion and backtracking

The function at the heart of solving the puzzle will be called `solve()`.
The function will be given a reference to the board data structure.  We
might also pass the X, Y coordinates of the cell that was just filled.
This helps us perform a scan for empty cells.  We can start searching
just after the filled cell.  Due to the scanning approach we know that
the just filled cell isn't empty, nor is any cell on the row to the left of
the filled cell, nor any cell above the "just filled" cell.

Once the function finds a new empty cell the code has to:

* generate all numbers unused in the group
* check all neighbour cells for that number

Once a number is found to be a valid entry in the cell the `puzzle` data structure
is updated with the new number, the `groups` dictionary is updated to add the new number
to the group set.  Then the `solve()` function is recursively called passing the
`puzzle` data structure and the just-placed X and Y coordinates.

# Finding a solution

We have found a solution if the scan for empty cells runs off the end of the
board.  This means that all cells are filled.  We print the board and then
return.  There may be multiple solutions and the search plus backtrack
approach wil find them all.

# Test puzzles

The test puzzles were taken from:

puzzle_1.sug - found somewhere on the 'net
puzzle_9.sug - https://files.krazydad.com/suguru/Stupendous_Suguru_Sampler.pdf


[The complete code for this section is here](Suguri.0r21.py).

A more complete "finished" program to find Suguru answers is
[in the *solver* directory](solver/solver.py).
