# Suguru

This ~~etude~~ project was started after answering a question on the /r/learnpython
subreddit:

https://old.reddit.com/r/learnpython/comments/1g6yylz/how_to_make_suguru_game_in_python/

My reply was along the lines of exhaustive search, backtracking, etc.
I also suggested the poster first tackle solving a Sudoku puzzle because
Sudoku can be solved with the same backtracking approach *without* the
extra complexity of handling random Suguru groups of cells.

This etude covers my journey attempting to solve Suguru puzzles using
python.

# Etude turned into a project

This project originally started as an etude, and I have left that word
in the remainder of the text.  The project that carries on after the 
etude is finished is split into two subdirectories:

* *solver* - a CLI program to solve a Suguru puzzle
* *editor* - a PyQt5 GUI program to create and edit Suguru data files
* *player* - a PyQt5 GUI program to play a game of Suguru

All programs use Suguru puzzle files produced by the *editor*.

# Suguru file format

The file *suguru_file_format.md* describes the format of a puzzle in a *\*.sug*
file.

# Test puzzles

There are a number of *\*.sug* files and associated *\*.soln* files that can be
tested with the *test_suguru.sh* test program.  That program looks for all
*\*.sug* files with an associated *\*.soln* file and tests that they
generate the correct output.

The *puzzle_1.sug* and the associated *puzzle_1.soln* came from
[this video on youtube](https://m.youtube.com/watch?v=BY5O1-R-c8o).

The files *puzzle_2.sug* and *puzzle_3.sug* are the first two puzzles
in *Suguru-10x10-Volume-1.pdf*.  That source also contains solutions.

The *puzzle_9.sug* is the first puzzle in the *Stupendous_Suguru_Sampler.pdf"*
file. Note that *solver.py* can't find a solution to that puzzle.  Don't know
if the puzzle is valid or not.

There are a couple of PDF files in the *resources* directory containing
more puzzles.

# On to the etude ...

[It starts here](Suguru.00.md).
