#!/usr/bin/env bash

#
# A bit of code to:
# * find all *.sug and matching *.soln files
# * run solver.py for each *.sug file, catching the output
# * check that the caught output matches the *.soln file
#

# the python executable file
sug_exe="solver.py"

usage()
{
	echo "Test the solver.py code."
	echo ""
	echo "    ./test_suguru.sh"
	echo ""
	exit 0
}

# check that we have no parameters
if [ $# -ne 0 ]; then
        usage
fi

# create a temporary file
tmpfile=$(mktemp "${TMPDIR:-/var/tmp/}${sug_stem}.XXXXXXXXXXXX")

# get all *.sug filenames, if matching *.son file run the test
all_sug_files=$(ls *.sug)

for sug_file in ${all_sug_files}; do
	sug_stem="${sug_file%.*}"
	sug_soln="${sug_stem}.soln"

	if [ -f "${sug_soln}" ]; then
		echo -n "Testing '${sug_file}' ... "
                ./${sug_exe} -q ${sug_file} >${tmpfile}
                
                # check that captured output is as expected
                if cmp -s "${tmpfile}" "${sug_soln}"; then
                        echo "got the wrong results:"
                	cat "${tmpfile}"
                	echo "-------------------- but expected:"
                	cat "${sug_soln}"
                	echo "--------------------"
		else
			echo "OK"
                fi

	fi
done

exit 0
