# Suguru solver

This program takes a file describing a Suguru puzzle and
prints out the solutions(s) to the puzzle, if any.

##Usage

    solver.py [-h] [-q] [-v] <puzzle_file>

    where  -h             prints this help and then stops
           -q             turns off the "More?" prompt after a solution
                          this is used for testing
           -v             print the puzzle after reading and then stop,
                          used to check that the *.sug file is correct
    and    <puzzle_file>  is the path to a puzzle description file
    
    The exit status of the program is standard: 0 means OK, else an error.

## test_suguru.sh

The *test_suguru.sh* file solves the puzle matching each *\*.soln* file
found and checks that each solution matches the data in the *\*.soln*
file.

Usage:

    bash test_suguru.sh
