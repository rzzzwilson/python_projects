#!/bin/env python

"""
Solve a Suguru puzzle from a description file.

Usage: solve.py [-h] [-q] [-v] <puzzle_file>

where  -h             prints this help and then stops
       -q             turns off the "More?" prompt after a solution
                      this is used for testing
       -v             print the puzzle after reading and then stop,
                      used to check that the *.sug file is correct
and    <puzzle_file>  is the path to a puzzle description file

The exit status of the program is standard: 0 means OK, else an error.
"""

import sys
import getopt
import traceback


def abort(msg):
    """Abort execution after printing the message."""

    print(msg)
    sys.exit(10)

def show_puzzle(puzzle, x_size, y_size):
    """Display the puzzle in a nice way.

    puzzle  the list of lists describing the puzzle
    x_size  X size of the puzzle
    y_size  Y size of the puzzle
    """

    # print the top edge line
    print("+", end="")  # the top-left corner
    for (i, cell) in enumerate(puzzle[0]):
        if i <= (x_size - 2):
            (group, _) = cell
            (right_group, _) = puzzle[0][i+1]
            print(f"---{'-' if group == right_group else '+'}", end="")
    print("---+")

    for (i, row) in enumerate(puzzle):
        print("|", end="") # start the cell lime

        # print cell contents and right-side divider of each cell
        for (j, cell) in enumerate(row):
            (group, value) = cell
            print(f" {value if value > 0 else '.'} ", end="")
            if j <= (x_size - 2):   # if there is a cell to the right
                (right_group, _) = row[j+1]
                print("|" if (right_group != group) else " ", end="")

        print("|")  # end the line

        # now print the following divider row if there is a cell row below
        if i <= (y_size - 2):   # if there is a puzzle row below
            (left_group, _) = row[0]
            (below_group, _) = puzzle[i+1][0]
            if left_group != below_group: # if first cell group and next first group not same
                print("+", end="")
            else:
                print("|", end="")

            for (j, cell) in enumerate(row):
                    (group, value) = cell 
                    (below_group, _) = puzzle[i+1][j]
                    print(f"{'---' if (below_group != group) else '   '}", end="")
                    if j <= (x_size - 2):
                        (right_group, _) = puzzle[i][j+1]
                        (below_right_group, _) = puzzle[i+1][j+1]
                        if (group == below_group and group == right_group
                                and group == below_right_group):
                            corner = " "
                        elif (group == below_group and right_group == below_right_group
                                and group != right_group):
                            corner = "|"
                        elif (group == right_group and below_group == below_right_group
                                and group != below_group):
                            corner = "-"
                        else:
                            corner = "+"
                        print(corner, end="")
            (left_group, _) = row[-1]
            (below_group, _) = puzzle[i+1][-1]
            print("+" if left_group != below_group else "|")

    # print the bottom edge line
    print("+", end="")  # the top-left corner
    for (i, cell) in enumerate(puzzle[-1]):
        if i <= (x_size - 2):
            (group, _) = cell
            (right_group, _) = puzzle[-1][i+1]
            print(f"---{'-' if group == right_group else '+'}", end="")
    print("---+")

def read_puzzle(filename):
    """Read the puzzle description from the given file.

    Returns a list of lists [group, value] describing
    the puzzle grid, a dictionary of group information, as well as
    the X and Y size.  If the data file isn't found return None.
    """

    # read the entire puzzle description file into list "data"
    try:
        with open(filename) as fd:
            data = fd.readlines()
    except FileNotFoundError:
        return None

    # check the first line of data to get the puzzle size
    sizes = data[0].strip()
    sizes_xy = sizes.split()
    if len(sizes_xy) != 2:
        abort(f"Bad format puzzle file, first line is '{sizes}'")
    (x_size, y_size) = sizes_xy
    try:
        x_size = int(x_size)
        y_size = int(y_size)
    except ValueError:
        abort(f"Bad format puzzle file, first line is '{sizes}'")
    if x_size < 1 or y_size < 1:
        abort(f"Bad format puzzle file, first line is '{sizes}'")

    # check we have correct number of data lines
    if len(data) != y_size + 1:
        abort(f"Bad format puzzle file, expected {y_size} data lines but got {len(data)-1}")

    # now read "y_size" lines
    # add to the "puzzle" list of lists
    # create "groups" dictionary: {0: (4, {2, 3}), 1: (5, {2, 4}), 2: (3, {})}
    puzzle = []
    groups = {}
    for i in range(y_size):
        line = data[i+1].strip()
        line_split = line.split()
        if len(line_split) != x_size:
            abort(f"Bad format puzzle file, line {i+2} should have "
                  f"{x_size} cells but got {len(line_split)}\n"
                  f"{line=}")

        # split cell description values into tuples of (group, value)
        row = []
        for cell in line_split:
            cell_split = cell.split(",")
            if len(cell_split) != 2:
                abort(f"Puzzle line {i+2} has bad cell description: {line}")
            (group, value) = cell_split
            try:
                group = int(group)
                value = int(value)
            except ValueError:
                abort(f"Puzzle line {i+2} has bad cell description: {line}")

            if group < 0 or value < 0:
                abort(f"Puzzle line {i+2} has bad cell description: {line}")

            row.append((group, value))

        puzzle.append(row)

        # add group and displayed number information to the "group" dictionary
        # each element in "row" is a tuple of (group, number)
        for cell in row:
            (group, display) = cell
            (group_size, shown) = groups.get(group, (0, set()))
            group_size += 1
            if display > 0:
                shown.update((display,))
            # now write back the updated value
            groups[group] = (group_size, shown)

    # done, return the "puzzle" list of lists
    return (puzzle, groups, x_size, y_size)

def neighbours(x, y, x_size, y_size):
    """Return neighbour cells to cell (x, y).

    Returns a list of coordinate tuples: [(x', y'), ...]
    """

    result = []

    # scan three neighbours above
    if y - 1 >= 0:
        for dx in {-1, 0, +1}:
            if x + dx >= 0 and x + dx < x_size:
                result.append((x+dx, y-1))

    # check left and right neighbours
    if x - 1 >= 0:
        result.append((x-1, y))
    if x + 1 < x_size:
        result.append((x+1, y))

    # scan three cells below
    if y + 1 < y_size:
        for dx in {-1, 0, +1}:
            if x + dx >= 0 and x + dx < x_size:
                result.append((x+dx, y+1))

    return result

def possible_numbers(puzzle, groups, x, y, x_size, y_size):
    """Return possible numbers to fill cell at x, y.

    Returns a list of possible numbers, or [] if none possible.
    """

    # check number show at (x,y)
    (group, show) = puzzle[y][x]
    if show != 0:       # if number shown
        return tuple()  #     then return empty tuple

    # get size of group and already shown numbers
    # return list of legal numbers not shown
    (group_size, group_shown) = groups[group]
    possibles = [x+1 for x in range(group_size) if x+1 not in group_shown]

    # now get all neighbouring numbers
    n_numbers = set()
    for (nx, ny) in neighbours(x, y, x_size, y_size):
        (_, nnum) = puzzle[ny][nx]
        if nnum: 
            n_numbers.update((nnum,))

    return [num for num in possibles if num not in n_numbers]

def place(puzzle, groups, number, x, y):
    """Place "number" at (x, y)."""

    # update the board
    (group, show) = puzzle[y][x]
    puzzle[y][x] = [group, number]

    # update the "groups" dictionary
    (_, shown_set) = groups[group]
    shown_set.update((number,))

def remove(puzzle, groups, number, x, y):
    """Remove "number" at (x, y)."""

    # update the board
    (group, show) = puzzle[y][x]
    puzzle[y][x] = (group, 0)

    # update the "groups" dictionary
    (_, shown_set) = groups[group]
    shown_set.remove(number)

def solve(puzzle, groups, x_size, y_size):
    """Just filled position (X, Y), find empty cell, recurse."""

    global num_solutions

    # find next empty cell, start at (0, 0), make more efficient later
    for y in range(y_size):
        for x in range(x_size):
            (group, show) = puzzle[y][x]
            if show == 0:
                possibles = possible_numbers(puzzle, groups, x, y, x_size, y_size)
                for number in possibles:
                    place(puzzle, groups, number, x, y)
                    solve(puzzle, groups, x_size, y_size)
                    remove(puzzle, groups, number, x, y)
                return  # we tried all possibles, so return to
                        #   allow previous call(s) to try other choices

    # if we get here there are no unfilled cells, we have a solution!
    show_puzzle(puzzle, x_size, y_size)
    num_solutions += 1
    if not be_quiet:
        input("More? ")

def suguru(filename, view_only):
    """Solve the suguru puzzle defined in the file.

    If 'view_only' is True then print the board after reading & stop.
    """

    # prepare the "puzzle" array
    data = read_puzzle(filename)
    if data is None:
        print(f"File '{filename}' doesn't exist!?")
        return

    (puzzle, groups, x_size, y_size) = data

    if view_only:
        show_puzzle(puzzle, x_size, y_size)
        return

    # start the process by calling "solve()"
    solve(puzzle, groups, x_size, y_size)

# to help the befuddled user
def usage(msg=None):
    if msg:
        print(f"{'*'*80}\n{msg}\n{'*'*80}")
    print(__doc__)

# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = f"\n{'=' * 80}"
    msg += "\nUncaught exception:\n"
    msg += "".join(traceback.format_exception(type, value, tb))
    msg += f"{'=' * 80}\n"
    print(msg)    # usually we would log the exception

# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]
try:
    (opts, args) = getopt.getopt(argv, "hqv", ["help", "quiet", "view"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

be_quiet = False    # a global, to control verbosity, assume talkative
view_only = False   # just print puzzle board if True
for (opt, param) in opts:
    if opt in ["-h", "--help"]:
        usage()
        sys.exit(0)
    elif opt in ["-q", "--quiet"]:
        be_quiet = True
    elif opt in ["-v", "--view"]:
        view_only = True

# check the filename
if len(args) != 1:
    usage("A single filename is required.")
    sys.exit(1)

filename = args[0]

# run the program code
num_solutions = 0
suguru(filename, view_only)
if not view_only:
    print(f"{num_solutions} solutions")
sys.exit(0)
