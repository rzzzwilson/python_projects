#!/bin/env python

"""
Suguru_01.py

Read a Suguru puzzle description file into memory.
Display the puzzle in a nice way.

Usage: Suguru.01.py <puzzle_file>

where  <puzzle_file>  is the path to a puzzle description file
"""

import sys


def abort(msg):
    """Abort execution after printing the message."""

    print(msg)
    sys.exit(10)

def show_puzzle(puzzle, x_size, y_size):
    """Display the puzzle in a nice way.

    puzzle  the list of lists describing the puzzle
    x_size  X size of the puzzle
    y_size  Y size of the puzzle
    """
    print(f"{puzzle=}")

    # print the top edge line
    print("+", end="")  # the top-left corner
    for (i, cell) in enumerate(puzzle[0]):
        if i <= (x_size - 2):
            (group, _) = cell
            (right_group, _) = puzzle[0][i+1]
            print(f"---{'-' if group == right_group else '+'}", end="")
    print("---+")

    for (i, row) in enumerate(puzzle):
        print("|", end="") # start the cell lime

        # print cell contents and right-side divider of each cell
        for (j, cell) in enumerate(row):
            (group, value) = cell
            print(f" {value if value > 0 else '.'} ", end="")
            if j <= (x_size - 2):   # if there is a cell to the right
                (right_group, _) = row[j+1]
                print("|" if (right_group != group) else " ", end="")

        print("|")  # end the line

        # now print the following divider row if there is a cell row below
        if i <= (y_size - 2):   # if there is a puzzle row below
            (left_group, _) = row[0]
            (below_group, _) = puzzle[i+1][0]
            if left_group != below_group: # if first cell group and next first group not same
                print("+", end="")
            else:
                print("|", end="")

            for (j, cell) in enumerate(row):
                    (group, value) = cell 
                    (below_group, _) = puzzle[i+1][j]
                    print(f"{'---' if (below_group != group) else '   '}", end="")
                    if j <= (x_size - 2):
                        (right_group, _) = puzzle[i][j+1]
                        (below_right_group, _) = puzzle[i+1][j+1]
                        if (group == below_group and group == right_group
                                and group == below_right_group):
                            corner = " "
                        elif (group == below_group and right_group == below_right_group
                                and group != right_group):
                            corner = "|"
                        elif (group == right_group and below_group == below_right_group
                                and group != below_group):
                            corner = "-"
                        else:
                            corner = "+"
                        print(corner, end="")
            (left_group, _) = row[-1]
            (below_group, _) = puzzle[i+1][-1]
            print("+" if left_group != below_group else "|")

    # print the bottom edge line
    print("+", end="")  # the top-left corner
    for (i, cell) in enumerate(puzzle[-1]):
        if i <= (x_size - 2):
            (group, _) = cell
            (right_group, _) = puzzle[-1][i+1]
            print(f"---{'-' if group == right_group else '+'}", end="")
    print("---+")


def read_puzzle(filename):
    """Read the puzzle description from the given file.

    Returns a list of lists of tuples (group, value) describing
    the puzzleI, as well as the X and Y size.
    """

    # read the entire puzzle description file into list "data"
    with open(filename) as fd:
        data = fd.readlines()

    # check the first line of data to get the puzzle size
    sizes = data[0].strip()
    sizes_xy = sizes.split()
    if len(sizes_xy) != 2:
        abort(f"Bad format puzzle file, first line is '{sizes}'")
    (x_size, y_size) = sizes_xy
    try:
        x_size = int(x_size)
        y_size = int(y_size)
    except ValueError:
        abort(f"Bad format puzzle file, first line is '{sizes}'")
    if x_size < 1 or y_size < 1:
        abort(f"Bad format puzzle file, first line is '{sizes}'")

    # check we have correct number of data lines
    if len(data) != y_size + 1:
        abort(f"Bad format puzzle file, expected {y_size} data lines but got {len(data)-1}")

    # now read "y_size" lines
    puzzle = []
    for i in range(y_size):
        line = data[i+1].strip()
        line_split = line.split()
        if len(line_split) != x_size:
            abort(f"Bad format puzzle file, line {i+2} should have "
                  f"{x_size} cells but got {len(line_split)}\n"
                  f"{line=}")

        # split cell description valuees into tuples of (group, contains)
        try:
            row = [(int(cell[:-1]), int(cell[-1])) for cell in line_split]
        except ValueError:
            abort(f"Puzzle line {i+2} has bad cell description: {line}")

        puzzle.append(row)

    # done, return the "puzzle" list of lists
    return (puzzle, x_size, y_size)

def suguru(filename):
    """Solev the suguru puzzle defined in the file."""

    # prepare the "puzzle" array
    (puzzle, x_size, y_size) = read_puzzle(filename)

    # print the "puzzle" list nicely
    show_puzzle(puzzle, x_size, y_size)

# parse the CLI params
if len(sys.argv) != 2:
    abort(f"{__doc__}")

filename = sys.argv[1]

# run the program code
suguru(filename)
