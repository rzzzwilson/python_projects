# The Suguru puzzle

*Suguru* appears to be a relatively new type of number puzzle,
similar to Sudoku.  The basic idea
[is defined in this page](https://www.puzzler.com/puzzles-a-z/suguru):

    A grid of cells is divided into outlined blocks containing up to five cells.
    Each cell in an outlined block contains a digit from 1 to n (where n is the
    number of cells in that block). So, a single-cell block contains the digit 1,
    a block of two cells contains the digits 1 and 2, and so on. The aim is to
    fill the grid so that no same digit is touching, not even diagonally.

The difficulty in solving this with a computer lies in the irregular and random
grouping of cells.  That is different for each game.  The first problem is the
biggest: how to define a suguru puzzle so we can read the data into a python
program?

# Puzzle representation in a file

We need to specify **two** things for each cell in the puzzle grid, the group
the cell is part of, and the number in that cell, if any.

We number groups from 1, so the first group we describe will be 1, the second
will be 2 and so on.  A group number of 0 shows that the cell is not part of
any group.  The number in a cell will follow the group number, with 0
indicating there is no number in the cell and 1 ... 5 being the actual number.
The group and value numbers are separated by a comma.

The puzzle description file will start with one line containing two
space-separated numbers.  The first number is the number of cells in the X
direction and the second number is the number of cells in the Y direction.

That first line is followed by lines describing each row in the puzzle (X direction).

The example puzzle description below shows a 6x6 puzzle:

    6 6
    1,0 1,0 2,1 2,0 2,0 3,0
    1,2 1,0 1,0 2,0 2,0 3,5
    4,0 4,0 4,0 5,0 5,0 6,0
    4,1 4,0 6,0 5,0 3,0 3,0
    7,0 6,0 6,0 6,0 8,0 8,0
    7,0 7,0 6,1 8,0 8,2 8,3

This is the puzzle described in
[this YouTube video](https://www.youtube.com/watch?v=BY5O1-R-c8o).

If a puzzle is large enough to have more than 9 groups then each cell number
in the description will have enough leading digits to describe the unique group
in the puzzle and be followed by a single digit, 0 ... 5, describing the number
in the cell.  So a puzzle may contain dell descriptions such as *03*, *105*,
*232* and so on.  A Suguru cell may only contain digits 1 ... 5 so only one
digit is ever needed for the cell contents.  The Suguru file format document
(*suguru_file_format.md*) expands that limitation to allow larger groups with
more than 5 cells if required.

# Reading a puzzle description file

Reading the data in a file into python memory is easy.  All we have to do is
read one line of cell values, convert that to an internal form such a list, 
and then append that "row" list to the final "puzzle" list.

Some thought should be given to making the operations we need to do to display
and solve the puzzle relatively easy to do.

When that's done properly we have a list of lists.  The elements of this array
are tuples of the form (group, value).  When we print the data loaded from 
*puzzle_1.sug* we see the loaded data:

    (1,0) (1,0) (2,1) (2,0) (2,0) (3,0)
    (1,2) (1,0) (1,0) (2,0) (2,0) (3,5)
    (4,0) (4,0) (4,0) (5,0) (5,0) (6,0)
    (4,1) (4,0) (6,0) (5,0) (3,0) (3,0)
    (7,0) (6,0) (6,0) (6,0) (8,0) (8,0)
    (7,0) (7,0) (6,1) (8,0) (8,2) (8,3)

This list is enough to solve the puzzle.

[The complete code for this part is here](Suguru.00.py).
In [the next part of the etude](Suguru.01.md), we print the puzzle in a nice
way on the console.
