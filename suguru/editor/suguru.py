"""
A custom widget for displaying/editing Suguru puzzles.

Events raised by the Suguru widget:

    * an editor save was performed
"""

import sys
import os.path
import copy
import math
from pprint import pformat

from PyQt5.QtCore import Qt, QPoint, QObject, QRect, pyqtSignal
from PyQt5.QtGui import (QPainter, QColor, QPen, QPainterPath, QKeyEvent, QFont,
                         QFontMetrics)
from PyQt5.QtWidgets import QWidget, QMenu, QMessageBox, QVBoxLayout, QLabel

from undo_redo import UndoRedo
import logger
log = logger.Log('suguru.log', logger.Log.DEBUG)


_version = "1.0"


###########################################################
# The custom widget displaying the cells
###########################################################

class Communicate(QObject):
    update_undoredo = pyqtSignal()


class Suguru(QWidget):

    # definitions for the Suguru widget
    GridColour = Qt.black                       # colour of the canvas grid lines
    GroupOutlineColour = QColor(255, 63, 63)    # group outline
    WidgetOutlineColour = Qt.blue               # the colour of outline around widget
    CanvasBG = QColor(242, 242, 242)            # the canvas background colour
    GroupBG = Qt.white                          # background of group cells
    TextColour = Qt.black                       # colour of value text
    MaxSize = 20                                # max dimension

    def __init__(self, parent=None, x_size=10, y_size=10, *args, **kwargs):
        super().__init__(*args, **kwargs)   # initialize the underlying QWidget

        self.version = _version             # make version visible

        # start a default project
        self.init_vars(x_size, y_size)
        self.maxsize = Suguru.MaxSize

        self.font_size = 10
        self.font = QFont('Terminus', self.font_size, QFont.Bold)

        self.editor = UndoRedo()            # start the undo/redo system
        self.c = Communicate()

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Suguru.CanvasBG)
        self.setPalette(p)

        # start mouse tracking
        self.setMouseTracking(True)

    def init_vars(self, x_size, y_size):
        """Initialize all state variables.

        x_size - X size of the new puzzle
        y_size - Y size of the new puzzle
        """

        self.x_size = x_size            # the X,Y size of the puzzle
        self.y_size = y_size

        self.width = 0                  # width of grid, set in the "paint" event
        self.height = 0                 # height of grid, set in the "paint" event
        self.grid_xspacing = 0          # X size of one cell, set in the "paint" event
        self.grid_yspacing = 0          # Y size of one cell, set in the "paint" event

        self.dragging = False           # True if mouse dragging, else False
        self.last_drag_x = 0            # previous X drag position
        self.last_drag_y = 0            # previous Y drag position

        self.next_group = 1             # number of next group
        self.recycled_groups = []       # recycled groups

        self.selected_cells = set()     # set of selected cells: {(cx,cy), ...}
        self.editing_group = None       # number of group if we are drag editing
        self.group2cells = {}           # dictionary of group numnber -> list of cells:
                                        #   {0: [(cx,cy), (cx',cy')], ...}
                                        #    ^    ^^^^^
                                        #    |      |
                                        #    |      +-- coords of cell in group
                                        #    +-- group number
        self.cell2group = {}            # dictionary mapping cell coords to group number
                                        #   {(cx,cy): <groupnum>, ...}
        self.cell2value = {}            # dictionary mapping cell to value

        self.shift_down = False         # True if SHIFT is held down

        self.current_file = None        # path to current project file
        self.project_name = "<empty>"   # basename form of project file path

        self.dirty = False              # True if unsaved data, ie, "dirty"

#####
# utility routines
#####

    def error(self, msg):
        """MessageBox to show a critical error."""

        mbox = QMessageBox(self)
        mbox.setIcon(QMessageBox.Critical)
        mbox.setText(msg)
        mbox.setWindowTitle("Error")
        mbox.setStandardButtons(QMessageBox.Ok)
        mbox.exec_()

    def dump_formatted(self, state):
        """Format state object nicely."""

        return pformat(state, width=60, compact=True)

    def editor_log(self, label, state):
        """Log the state of the undo/redo system and current state."""

        log(f"{label} current state is:")
        log(self.dump_formatted(state))
        log("~" * 70)
        log("undo/redo system state:")
        log(self.editor.dump())
        log("~" * 70)

    def pack_state(self):
        """Pack the state into a sequence and return it."""

        state = (self.x_size, self.y_size, self.width, self.height,
                 self.grid_xspacing, self.grid_yspacing,
                 self.next_group, self.recycled_groups,
                 self.selected_cells, self.editing_group,
                 self.group2cells, self.cell2group, self.cell2value,
                 self.current_file, self.project_name, self.dirty)
        return copy.deepcopy(state)

    def unpack_state(self, state):
        """Unpack the state into the state variables."""

        (self.x_size, self.y_size, self.width, self.height,
         self.grid_xspacing, self.grid_yspacing,
         self.next_group, self.recycled_groups,
         self.selected_cells, self.editing_group,
         self.group2cells, self.cell2group, self.cell2value,
         self.current_file, self.project_name, self.dirty) = state

    def debug(self):
        """Dump some debug information."""

        print("-------------------------------")
        print(f"{self.selected_cells=}")
        print(f"{self.group2cells=}")
        print(f"{self.cell2group=}")
        print(f"{self.cell2value=}")
        print("-------------------------------")
        print(f"{self.editor.dump()}")
        print("-------------------------------")

    def next_group_number(self):
        """Return an unused group number.

        Check recycled numbers before issuing new one.
        """

        if self.recycled_groups:
            return self.recycled_groups.pop()

        result = self.next_group
        self.next_group += 1
        return result

    def recycle_grpnum(self, grpnum):
        """Put recycled group number into 'recycled_groups' list."""

        self.recycled_groups.append(grpnum)

    def s2g_coords(self, sx, sy):
        """Convert screen coords to grid cell coords."""

        w_size = self.width // self.x_size
        h_size = self.height // self.y_size
        gx = math.ceil(sx // w_size)
        gy = math.ceil(sy // h_size)

        return (gx, gy)

    def g2s_coords(self, gx, gy):
        """Convert grid cell coords to screen coords."""

        w_size = self.width // self.x_size
        h_size = self.height // self.y_size
        sx = gx * w_size
        sy = gy * h_size

        return (sx, sy)

    def joined2group(self, cell, group):
        """Test if "cell" edge-joins any cell in "group"."""
    
        (cx, cy) = cell
    
        for dx in (-1, +1):             # test if joins E-W
            if (cx+dx, cy) in group:
                return True
    
        for dy in (-1, +1):             # test if joins N-S
            if (cx, cy+dy) in group:
                return True
    
        return False
    
    def find_groups(self, grpnum):
        """Take all cells in group "grpnum", return a list of lists of groups.

        grpnum  the group number of all the cells to consider

        Basic approach is to check each cell against every group in "result".
        If the cell joins, put all cells in group into "joined_cells".  Keep
        track of the groups we need to remove from "result".  After checking
        the cell against all groups append the cell to "joined_cells".  If
        there were no groups joined then "cell" is all by itself and starts
        a new group.  Then remove all joined groups from "result" and append
        the joined/new group to "result".
        """
    
        # if the group was just deleted, return 'no cells'
        if grpnum not in self.group2cells:
            return []

        cells = self.group2cells[grpnum]         # get all cells to consider

        result = []                              # the returned list
    
        for cell in cells:
            joined_cells = []                    # joined groups list
            removed_groups = []                  # list of removed groups
            for group in result:
                if self.joined2group(cell, group):    
                    removed_groups.append(group) # cell joins group, remove group
                    joined_cells.extend(group)   # flatten into joined groups
            joined_cells.append(cell)            # add the cell to the cells
            for g in removed_groups:             # remove joined groups from "result"
                result.remove(g)
            result.append(joined_cells)          # append the joined cells as a new group
    
        return result

    def renumber_groups(self, orig_grpnum, groups):
        """Renumber multiple groups if any in 'groups'.

        orig_grpnum  is the original group number for all cells
        groups       is a list of lists of joined cells.

        If list is empty or there is only one joined sublist do nothing.
        Remove any cell values in all groups.
        """

        if len(groups) < 2:
            return

        # get the original group
        orig_group_cells = self.group2cells[orig_grpnum]

        # delete all values in original group
        for cell in orig_group_cells:
            if cell in self.cell2value:
                del self.cell2value[cell]

        # renumber extra groups
        for new_group in groups[1:]:
            # get new group number 
            new_grpnum = self.next_group_number()

            # remove all new group cells from the original group
            for cell in new_group:
                orig_group_cells.remove(cell)

            # update cell->group mapping to match new group number
                self.cell2group[cell] = new_grpnum

            # add list of cells to group->cells dict
            self.group2cells[new_grpnum] = new_group

    def near_values(self, cell):
        """Return a set of values in cells around "cell", all directions."""

        result = set()

        (cx, cy) = cell
        for x in range(-1, 2):
            for y in range(-1, 2):
                other_cell = (cx+x, cy+y)
                if other_cell != cell:
                    # does neighbour cell have a value?
                    if other_cell in self.cell2value:
                        result.add(self.cell2value[other_cell])

        return result

###########################################################

    def editor_undo(self):
        """Save current state to redo_list and return popped undo_list."""

        if self.editor.can_undo():
            state = self.pack_state()
            new_state = self.editor.undo(state)
            self.unpack_state(new_state)

            self.editor_log("editor_undo: after: ", new_state)

    def editor_redo(self):
        """Redo and unpack next redo state."""

        if self.editor.can_redo():
            state = self.pack_state()
            new_state = self.editor.redo(state)
            self.unpack_state(new_state)

            self.editor_log("editor_redo: after: ", new_state)

    def calc_font_size(self):
        """Calculate a proper font size for the cell numbers'"""

        max_number = self.x_size * self.y_size
        text = f"{max_number}"
        self.font_size = 5
        min_spacing = min(self.grid_xspacing, self.grid_yspacing)

        while True:
            font = QFont('Terminus', self.font_size, QFont.Bold)
            fm = QFontMetrics(font)
            width = fm.width(text)
            if width < min_spacing*4//5:
                self.font_size += 1
            else:
                self.font_size -= 1
                break
        self.font = QFont('Terminus', self.font_size, QFont.Bold)

    def resizeEvent(self, event=None):
        """Window resize, recalculate the number size."""

        size = self.size()
        self.width = size.width()
        self.height = size.height()
        self.grid_xspacing = self.width // self.x_size
        self.grid_yspacing = self.height // self.y_size

        self.calc_font_size()

    def new_file(self, x_size, y_size, save=True):
        """Initialize all state variables to "new" state.

        x_size - X dimension of the new puzzle
        y_size - Y dimension of the new puzzle
        save   - False if we don't need to save the current state
        """

        # save the prior state, push to undo later maybe
        if save:
            prior_state = self.pack_state()
            self.editor_log("new_file: before: ", prior_state)

        # set state variables
        self.x_size = x_size
        self.y_size = y_size

        log(f"new_file: creating a new file: {self.x_size=} x {self.y_size}")

        self.width = 0                  # width of grid, set in the "paint" event
        self.height = 0                 # height of grid, set in the "paint" event
        self.grid_xspacing = 0          # X size of one cell, set in the "paint" event
        self.grid_yspacing = 0          # Y size of one cell, set in the "paint" event

        self.dragging = False           # True if mouse dragging, else False
        self.last_drag_x = 0            # previous X drag position
        self.last_drag_y = 0            # previous Y drag position

        self.next_group = 1             # number of next group
        self.recycled_groups = []       # recycled groups

        self.selected_cells = set()     # set of selected cells: {(cx,cy), ...}
        self.editing_group = None       # number of group if we are drag editing
        self.group2cells = {}           # dictionary of group numnber -> list of cells:
                                        #   {0: [(cx,cy), (cx',cy')], ...}
                                        #    ^    ^^^^^
                                        #    |      |
                                        #    |      +-- coords of cell in group
                                        #    +-- group number
        self.cell2group = {}            # dictionary mapping cell coords to group number
                                        #   {(cx,cy): <groupnum>, ...}
        self.cell2value = {}            # dictionary mapping cell to value

        self.shift_down = False         # True if SHIFT is held down

        self.current_file = None        # path to current project file
        self.project_name = "<empty>"   # basename form of project file path

        self.dirty = False              # True if unsaved data, ie, "dirty"

        # resize event also sets appropriate font size
        self.resizeEvent()

        # save prior state to undo/redo
        if save:
            self.editor.save(prior_state)
            self.editor_log("new_file: after: ", self.pack_state())

        self.repaint()

    def open_file(self, filename):
        """Read the puzzle in *.sug form.

        filename  the path to the file to read

        We have to read all data into local variables first.
        If there's no error copy locals to widget variables.
        """
   
        # make a short filename string for reporting purposes
        file_base = os.path.basename(filename)

        # save the prior state, use it on a good "open"
        prior_state = self.pack_state()

        log(f"open_file: opening '{file_base}'")
        state = self.pack_state()
        self.editor_log("open_file: before: ", state)

        # read the file
        with open(filename) as fd:
            data = fd.readlines()

        if len(data) < 2:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        # get X and Y size from first line
        sizes = data[0].split()
        data = data[1:]
        if len(sizes) != 2:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        try:
            sizes = [int(x) for x in sizes]
        except ValueError:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        (x_size, y_size) = sizes    # the size of the new puzzle
        group2cells = {}            # set locals to "empty" values
        cell2group = {}
        cell2value = {}
        selected_cells = set()

        if x_size < 1 or y_size < 1:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        if x_size > self.maxsize or y_size > self.maxsize:
            self.error(f"File {file_base} data is too big.")
            return

        if len(data) != y_size:
            self.error(f"File {file_base} isn't a valid Suguru file.")
            return

        max_group = 0
        for (row, line) in enumerate(data):
            line = line.split()
            if len(line) != x_size:
                self.error(f"File {file_base} isn't a valid Suguru file.")
                return
            for (col, item) in enumerate(line):
                fields = item.split(",")
                if len(fields) != 2:
                    self.error(f"File {file_base} isn't a valid Suguru file.")
                    return
                try:
                    fields = [int(item) for item in fields]
                except ValueError:
                    self.error(f"File {file_base} isn't a valid Suguru file.")
                    return

                (grpnum, value) = fields    # remember largest group number
                if grpnum > max_group:
                    max_group = grpnum

                cell = (col, row)
                if grpnum > 0:
                    if grpnum not in group2cells:
                        group2cells[grpnum] = []
                    group2cells[grpnum].append(cell)
                    cell2group[cell] = grpnum
                    selected_cells.add(cell)
                if value > 0:
                    cell2value[cell] = value

        ######
        # Now update the widget variables
        ######

        self.x_size = x_size
        self.y_size = y_size
        self.group2cells = group2cells
        self.cell2group = cell2group
        self.cell2value = cell2value
        self.selected_cells = selected_cells

        # set the "next group" mechanism up
        # this leaves "holes" in the groups that can be allocated
        # could try to identify group nunmbres < max_group that aren't used?
        self.next_group = max_group + 1
        self.recycled_groups = []

        # set the project filepath
        self.current_file = filename
        (self.project_name, _) = os.path.splitext(os.path.basename(filename))

        # reset the "dirty" flag
        self.dirty = False

        # set the appropriate font size
        self.resizeEvent()

        # "open" successful, save prior state to undo/redo
        self.editor.save(prior_state)
        self.editor_log("open_file: after: ", self.pack_state())

    def save_file(self, filename):
        """Save the puzzle in *.sug form.

        filename  the path to the file to write
        """

        # make a short filename string for reporting purposes
        file_base = os.path.basename(filename)

        log(f"Saving to file: '{file_base}'")

        # get maximum group number, figure max group string width
        max_grp = 1
        if self.group2cells:
            max_grp = max(self.group2cells.keys())
        max_glen = len(f"{max_grp}")

        # get maximum value, figure max value string width
        max_val = 1
        if self.cell2value:
            max_val = max(self.cell2value.values())
        max_vlen = len(f"{max_val}")

        # write data to the file
        with open(filename, "w") as fd:
            fd.write(f"{self.x_size} {self.y_size}\n")
            for row in range(self.y_size):
                for col in range(self.x_size):
                    cell = (col, row)
                    group = self.cell2group.get(cell, 0)
                    cell_value = self.get_cell_value(cell)
                    fd.write(f"{group:{max_glen}d},{cell_value:<{max_vlen}d} ")

                fd.write("\n")

        # set the project filepath
        self.current_file = filename
        (self.project_name, _) = os.path.splitext(os.path.basename(filename))

        # reset the "dirty" flag
        self.dirty = False

#####
# drawing methods
#####

    def draw_cell_outline(self, qp, x, y, side):
        """Draw a group "edge" line for a grid cell.

        qp    the canvas to draw on
        x, y  the cell coordinates
        side  one of "t", "b", "l" or "r" (the side)
        """

        # get top-left coordinate of cell from cell x,y
        tl_x = self.grid_xspacing * x
        tl_y = self.grid_yspacing * y

        # draw thicker side outline
        if side == "t":
            start_x = tl_x
            start_y = tl_y
            stop_x = start_x + self.grid_xspacing
            stop_y = start_y
        elif side == "r":
            start_x = tl_x + self.grid_xspacing
            start_y = tl_y
            stop_x = start_x
            stop_y = start_y + self.grid_yspacing
        elif side == "b":
            start_x = tl_x
            start_y = tl_y + self.grid_yspacing
            stop_x = start_x + self.grid_xspacing
            stop_y = start_y
        elif side == "l":
            start_x = tl_x
            start_y = tl_y
            stop_x = start_x
            stop_y = start_y + self.grid_yspacing
        else:
            raise ValueError(f"Got illegal 'side' value: {side}")

        qp.drawLine(start_x, start_y, stop_x, stop_y)

#####
# 
#####

    def edit_value(self, value, cell):
        """Set the value of a cell. 0 means remove value."""

        # set value of cell to "value", 0 means remove value
        if value == 0:
            if cell in self.cell2value:
                del self.cell2value[cell]
                return True
        else:
            self.cell2value[cell] = value
            return True
        return False

    def select_cell(self, cell, grpnum):
        """Add "cell" to the "grpnum" group."""

        self.selected_cells.add(cell)
        self.cell2group[cell] = grpnum
        if grpnum in self.group2cells:
            self.group2cells[grpnum].append(cell)
        else:
            self.group2cells[grpnum] = [cell]

    def deselect_cell(self, cell):
        """Remove cell from group."""

        self.selected_cells.remove(cell)
        grpnum = self.cell2group[cell]
        self.group2cells[grpnum].remove(cell)
        if not self.group2cells[grpnum]:    # if list of cells is empty
            del self.group2cells[grpnum]    #    delete entire key+value
            self.recycle_grpnum(grpnum)
        del self.cell2group[cell]
        if cell in self.cell2value:         # also remove any value
            del self.cell2value[cell]

    def set_cell_value(self, cell, value):
        """Set "value" in "cell"."""

        self.cell2value[cell] = value

    def get_cell_value(self, cell):
        """Get the value in "cell"."""

        return self.cell2value.get(cell, 0)

#####
# over-ridden widget methods
#####

    def mousePressEvent(self, event):
        """Catch mouse button DOWN event."""

        # get current mouse position
        mx = event.x()
        my = event.y()

        # the button that was pressed
        button = event.button()

        # figure out what cell was clicked from click screen coords
        (xindex, yindex) = self.s2g_coords(mx, my)

        # save prior state
        prior_state = self.pack_state()

        # if left DOWN select cell
        if button == Qt.LeftButton:
            self.last_drag_x = mx                       # prepare for possible drag
            self.last_drag_y = my
            self.dragging = True

            cell = (xindex, yindex)
            if self.shift_down:
                grpnum = self.cell2group.get(cell, 0)   # get current group number
                if grpnum == 0:                         # if 0, not on selected cell
                    return

                # remove a selected cell
                self.deselect_cell(cell)

                # check if deletion split group, renumber if so
                joined_groups = self.find_groups(grpnum)
                self.renumber_groups(grpnum, joined_groups)
                self.dirty = True

                # deselected cell, save
                self.editor.save(prior_state)
                self.c.update_undoredo.emit()   # tell app code to update undo/redo menu
            else:
                # no shift, selecting cells
                if cell in self.selected_cells:         # if cell already selected
                    self.editing_group = self.cell2group[cell]
                else:                                   # not selected, start of new group
                    grpnum = self.next_group_number()   # number of the new group
                    self.select_cell(cell, grpnum)
                    self.editing_group = grpnum
                    self.dirty = True

                    # selected cell, save
                    self.editor.save(prior_state)
                    self.c.update_undoredo.emit()   # tell app code to update undo/redo menu
        elif button == Qt.RightButton:
            # check if right clicked cell is in group
            cell = self.s2g_coords(mx, my)
            grpnum = self.cell2group.get(cell, 0)
            context_menu = QMenu(self)
            if grpnum:
                # clicked on a cell in a group
                # get size of group and values already set in group
                cell_list = self.group2cells[grpnum]
                num_cells = len(cell_list)
                used_values = set(self.cell2value.get(c, 0) for c in cell_list)

                # create the context menu, exclude numbers already in this group
                # and any value in immediate area regardless of group
                a = context_menu.addAction("delete")
                if not self.cell2value.get(cell, 0):
                    a.setEnabled(False)
                a.triggered.connect(lambda chk, x=0, c=cell: self.edit_value(x, c))
                for anum in range(1, num_cells+1):
                    a = context_menu.addAction(f"insert {anum}")
                    a.triggered.connect(lambda chk, x=anum, c=cell: self.edit_value(x, c))
                    if anum in used_values or anum in self.near_values(cell):
                        a.setEnabled(False)

                # display context menu around the click point
                result = context_menu.exec(self.mapToGlobal(event.pos()))

                # if edited cell, save
                if result:
                    self.editor.save(prior_state)
                    self.c.update_undoredo.emit()   # tell app code to update undo/redo menu
        elif button == Qt.MidButton:
            pass
#            print(f"mousePressEvent: Middle: {mx=}, {my=}, {event=}")

        # show changes to the view, if any
        self.repaint()

    def mouseReleaseEvent(self, event):
        """Catch mouse button UP event."""

        # get current mouse position
        mx = event.x()
        my = event.y()

        # the button that was pressed
        button = event.button()

        # if left button up then ...
        if button == Qt.LeftButton:
            self.dragging = False
            self.editing_group = None
        elif button == Qt.RightButton:
            pass
#            print(f"mouseReleaseEvent: Right: {mx=}, {my=}, {event=}")
        elif button == Qt.MidButton:
            pass
#            print(f"mouseReleaseEvent: Middle: {mx=}, {my=}, {event=}")

    def mouseMoveEvent(self, event):
        """Catch a mouse move event.

        If mouse dragging, edit a group.
            NO SHIFT - add to group
            SHIFT    - delete from groups
        """

        # get current mouse position
        mx = event.x()
        my = event.y()

        actual_width = self.x_size*self.grid_xspacing
        actual_height = self.y_size*self.grid_yspacing

        # check that we are on the canvas
        if (mx < 0 or mx >= actual_width
            or my < 0 or my >= actual_height):
            return

        # handle dragging
        if self.dragging:
            prior_state = self.pack_state()

            # get delta move from last drag move
            cell = self.s2g_coords(mx, my)

            # if both cell X and Y coords changed from last drag
            # IGNORE IT and turn off dragging since it's a diagonal move
            prev_cell = self.s2g_coords(self.last_drag_x, self.last_drag_y)
            (cx, cy) = cell
            (pcx, pcy) = prev_cell
            if abs(cx - pcx) and abs(cy - pcy):
                self.dragging = False
                return

            if self.shift_down:
                # if cell we are hovering over is selected
                grpnum = self.cell2group.get(cell, 0)
                if cell in self.selected_cells:
                    self.deselect_cell(cell)

                    # check if deletion split group, renumber if so
                    joined_groups = self.find_groups(grpnum)
                    self.renumber_groups(grpnum, joined_groups)

                    self.editor.save(prior_state)
                    self.c.update_undoredo.emit()   # tell app code to update undo/redo menu
            else:
                # check cell we are hovering over
                # if already selected, do nothing
                if cell in self.selected_cells:
                    # check group of this selected cell
                    # if different from dragging group, cancel dragging
                    if self.cell2group[cell] != self.editing_group:
                        self.dragging = False
                        return
                else:
                    # otherwise, select cell
                    self.select_cell(cell, self.editing_group)
    
                    self.editor.save(prior_state)
                    self.c.update_undoredo.emit()   # tell app code to update undo/redo menu
            # update last drag move values
            self.last_drag_x = mx
            self.last_drag_y = my

            self.dirty = True

        # show changes to the view, if any
        self.repaint()

    def keyPressEvent(self, event):
        """Capture a key press."""

        key = event.key()
        if key == Qt.Key_Shift:
            self.shift_down = True

    def keyReleaseEvent(self, event):
        """Capture a key release."""

        key = event.key()
        if key == Qt.Key_Shift:
            self.shift_down = False

    def paintEvent(self, e):
        """Handle the widget 'paint' event.

        Check widget size, then draw objects on the canvas.
        """

        qp = QPainter()
        qp.begin(self)

        size = self.size()
        self.width = size.width()
        self.height = size.height()
        self.grid_xspacing = self.width // self.x_size
        self.grid_yspacing = self.height // self.y_size

        qp.setRenderHint(QPainter.Antialiasing)

        actual_width = self.x_size*self.grid_xspacing
        actual_height = self.y_size*self.grid_yspacing

        # draw grid
        pen = QPen(Suguru.GridColour, 0.5, Qt.DotLine)
        qp.setPen(pen)
        qp.setBrush(Suguru.CanvasBG)
        for x in range(self.grid_xspacing, self.width, self.grid_xspacing):
            qp.drawLine(x, 0, x, actual_height)
        for y in range(self.grid_yspacing, self.height, self.grid_yspacing):
            qp.drawLine(0, y, actual_width, y)

        # draw the group background colour
        pen = QPen(Suguru.GridColour, 0, Qt.NoPen)
        qp.setPen(pen)
        qp.setBrush(Suguru.GroupBG)
        for ((gx, gy), _) in self.cell2group.items():
            (sx, sy) = self.g2s_coords(gx, gy)
            qp.drawRect(sx+1, sy+1,     # leave a little outline of background
                        self.grid_xspacing-1, self.grid_yspacing-1)

        # draw canvas outline
        pen = QPen(Suguru.WidgetOutlineColour, 2, Qt.SolidLine)
        qp.setPen(pen)
        actual_width = self.x_size*self.grid_xspacing
        actual_height = self.y_size*self.grid_yspacing
        qp.drawLine(0, 0, actual_width, 0)                          # top
        qp.drawLine(0, actual_height, actual_width, actual_height)  # bottom
        qp.drawLine(actual_width, actual_height, actual_width, 0)   # right
        qp.drawLine(0, 0, 0, actual_height)                         # left

        # draw outlines around groups, draw group BG colour
        pen = QPen(Suguru.GroupOutlineColour, 1, Qt.SolidLine)
        qp.setPen(pen)
        for ((x, y), grpnum) in self.cell2group.items():
            # check all neighbours of this cell
            if self.cell2group.get((x, y-1), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "t")
            if self.cell2group.get((x, y+1), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "b")
            if self.cell2group.get((x-1, y), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "l")
            if self.cell2group.get((x+1, y), 0) != grpnum:
                self.draw_cell_outline(qp, x, y, "r")

        # put values into cells that have them
        pen = QPen(Suguru.TextColour, 0)
        qp.setPen(pen)
        qp.setFont(self.font)
        for (cell, value) in self.cell2value.items():
            (x, y) = cell                       # cell coords
            rect = QRect(x*self.grid_xspacing, y*self.grid_yspacing, self.grid_xspacing, self.grid_yspacing)
            qp.drawText(rect, Qt.AlignHCenter|Qt.AlignVCenter, f"{value}")

        qp.end()
