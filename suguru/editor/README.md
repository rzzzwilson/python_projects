# Suguru editor

This is a GUI program to create and edit Suguru puzzles. Run it by:

    python editor.py

It reads/writes the *\*.sug* files describing a Suguru puzzle.
The editor allows:

* Creation of new puzzles
* Display of existing puzzles
* Edit of existing puzzles

The basic operations allowed are:

* Place a digit in a group, with checks on valid numbers for the size of groups, etc
* Remove a value from a group
* Defining a group in a "drag" mode where dragging the cursor expands the group
* Deleting cells in a group: shift+drag

The editor has the usual basic functionality:

* Save
* Load
* Undo
* Redo
* and so on

## Basic layout

The main editor display is an NxM grid of cells.

There will be a display_only area that shows information on the selected cell,
group, group size, number if any, etc.

## "grid" widget

Left-down and drag will add cells to group.  If the left-down is in an existing
group the group is extended.  If not, a new group is started.

Shift down when doing left-down+drag deletes cells from an existing group.
No operation if left-down isn't in an existing group.

## Future goals

The editor could be expanded to:

* estimate difficulty of suguru (how?)
* allow user to actually play the game
* autosolve a suguru (single solution, multiple, none)
* when solving, choose show steps or not
