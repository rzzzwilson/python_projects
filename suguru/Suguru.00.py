#!/bin/env python

"""
Read a Suguru puzzle description file into memory.

Usage: Suguru.00.py <puzzle_file>

where  <puzzle_file>  is the path to a puzzle description file
"""

import sys


def abort(msg):
    """Abort execution after printing the message."""

    print(msg)
    sys.exit(10)

def read_puzzle(filename):
    """Read the puzzle description from the given file.

    Returns a list of lists of tuples (group, value) describing
    the puzzle.
    """

    # read the entire puzzle description file into list "data"
    with open(filename) as fd:
        data = fd.readlines()

    # check the first line of data to get the puzzle size
    sizes = data[0].strip()
    sizes_xy = sizes.split()
    if len(sizes_xy) != 2:
        abort(f"Bad format puzzle file, first line is '{sizes}'")
    (x_size, y_size) = sizes_xy
    try:
        x_size = int(x_size)
        y_size = int(y_size)
    except ValueError:
        abort(f"Bad format puzzle file, first line is '{sizes}'")
    if x_size < 1 or y_size < 1:
        abort(f"Bad format puzzle file, first line is '{sizes}'")

    # check we have correct number of data lines
    if len(data) != y_size + 1:
        abort(f"Bad format puzzle file, expected {y_size} data lines but got {len(data)-1}")

    # now read "y_size" lines
    puzzle = []
    for i in range(y_size):
        line = data[i+1].strip()
        line_split = line.split()
        if len(line_split) != x_size:
            abort(f"Bad format puzzle file, line {i+2} should have "
                  f"{x_size} cells but got {len(line_split)}\n"
                  f"{line=}")

        # split cell description valuees into tuples of (group, contains)
        try:
            row = [(int(cell[:-1]), int(cell[-1])) for cell in line_split]
        except ValueError:
            abort(f"Puzzle line {i+2} has bad cell description: {line}")

        puzzle.append(row)

    # done, return the "puzzle" list of lists
    return puzzle

# the suguru solving code goes here
def suguru(filename):
    # prepare the "puzzle" array
    puzzle = read_puzzle(filename)

    # DEBUG
    # print the "puzzle" list nicely
    print("puzzle:")
    for row in puzzle:
        for cell in row:
            print(f"{cell} ", end="")
        print()

    return 0

# parse the CLI params
if len(sys.argv) != 2:
    abort(f"{__doc__}")

filename = sys.argv[1]

# run the program code
suguru(filename)
