#!/usr/bin/env python

"""
Implement a tkinter program to view Henon maps:

    https://en.wikipedia.org/wiki/H%C3%A9non_map

"""

from tkinter import Tk, Canvas, StringVar
from tkinter.ttk import Frame, Label, Entry, Button


x_scale = 300
y_scale = 500

class Henon(Frame):
    def __init__(self):
        super().__init__()
        self.initUI()
        self.run_flag = False

    def initUI(self):

        self.master.title("Hénon")
#        self.centerWindow()
        self.grid()

        self.a_sv = StringVar()
        a_label = Label(self, text="a: ")
        a_label.grid(row=0, column=0)
        a_text = Entry(self, textvariable=self.a_sv)
        a_text.grid(row=0, column=1)

        self.b_sv = StringVar()
        b_label = Label(self, text="b: ")
        b_label.grid(row=1, column=0)
        b_text = Entry(self, textvariable=self.b_sv)
        b_text.grid(row=1, column=1)

        self.x_sv = StringVar()
        x_label = Label(self, text="x: ")
        x_label.grid(row=2, column=0)
        x_text = Entry(self, textvariable=self.x_sv)
        x_text.grid(row=2, column=1)

        self.y_sv = StringVar()
        y_label = Label(self, text="y: ")
        y_label.grid(row=3, column=0)
        y_text = Entry(self, textvariable=self.y_sv)
        y_text.grid(row=3, column=1)

        self.run_button = Button(self, text="Run", command=self.run)
        self.run_button.grid(row=4, column=0, columnspan=2)
        self.run_button["state"] = "disabled"

        clear_button = Button(self, text="Clear", command=self.clear)
        clear_button.grid(row=5, column=0, columnspan=2)

        self.save_button = Button(self, text="Save", command=self.save)
        self.save_button.grid(row=6, column=0, columnspan=2)
        self.save_button["state"] = "disabled"

        self.canvas = Canvas(self, background="white", height=600, width=1000)
        self.canvas.grid(row=0, column=2, rowspan=10)

        self.a_sv.trace("w", self.check_abxy)
        self.b_sv.trace("w", self.check_abxy)
        self.x_sv.trace("w", self.check_abxy)
        self.y_sv.trace("w", self.check_abxy)

    def centerWindow(self):
        w = 1200
        h = 700

        sw = self.master.winfo_screenwidth()
        sh = self.master.winfo_screenheight()

        x = (sw - w)/2
        y = (sh - h)/2
        self.master.geometry('%dx%d+%d+%d' % (w, h, x, y))

    def check_abxy(self, *args):
        """If "a", "b", "x" and "y" Entry widgets all contain floats, enable "Run" button."""

        self.run_button["state"] = "disabled"
        self.save_button["state"] = "disabled"

        try:
            float(self.a_sv.get())
        except ValueError:
            return

        try:
            float(self.b_sv.get())
        except ValueError:
            return

        try:
            float(self.x_sv.get())
        except ValueError:
            return

        try:
            float(self.y_sv.get())
        except ValueError:
            return

        self.run_button["state"] = "enable"
        self.save_button["state"] = "enabled"

    def one_step(self):
        """Do one step of animation.  Stop if "run_flag" goes False."""

        if not self.run_flag:
            self.run_button["text"] = "Run"
            return

        new_x = 1 - self.a*self.x*self.x + self.y
        new_y = self.b*self.x

        # convert to canvas coordinates
        canvas_x = new_x*x_scale + 500
        canvas_y = new_y*y_scale + 300

        point = (canvas_x-1, canvas_y-1, canvas_x+1, canvas_y+1)
        self.canvas.create_oval(point, fill="black")

        self.x = new_x
        self.y = new_y

        root.after(1, self.one_step)

    def run(self):
        if self.run_button["text"] == "Run":
            self.run_flag = True
            self.a = float(self.a_sv.get())
            self.b = float(self.b_sv.get())
            self.x = float(self.x_sv.get())
            self.y = float(self.y_sv.get())
            self.run_button["text"] = "Stop"
            root.after(0, self.one_step)
        else:
            self.run_flag = False
            self.run_button["text"] = "Run"

    def clear(self):
        self.canvas.delete('all')

    def save(self):
        filename = f"henon_{self.a:.4f}_{self.b:.4f}.ps"
        self.canvas.postscript(file=filename, colormode='color')


root = Tk()
app = Henon()
root.mainloop()
