The code here was written after responding to a reddit question:

https://old.reddit.com/r/learnpython/comments/17ivaf6/how_do_i_proceed_with_these_questions/

The aim of the code is to implement the Henon map with tkinter.  Allow
variation of the "a" and "b" parameters.  Try to make the interface as
interactive as possible.
